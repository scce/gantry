# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0]

### Added

- Support for container commands
- Support for container entrypoints

### Fixed

- Unsuccessful builds of Dockerimages failed silently

## [0.2.0]

### Added

- Support for named volumes

### Changed

- Support Dockerfiles at the root folder of a project

## [0.1.0]

### Added

- Basic image management (start, stop)
- Basic container management (start, stop and remove)
- Basic container configuration (env variables, links, ports, volumes)
- Implemented for Docker engine

[0.3.0]: https://gitlab.com/scce/gantry/-/tags/v0.3.0
[0.2.0]: https://gitlab.com/scce/gantry/-/tags/v0.2.0
[0.1.0]: https://gitlab.com/scce/gantry/-/tags/v0.1.0

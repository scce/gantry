# Gantry

Library to orchestrate containers for local development environments directly out of Java.

**Motivation**.
Local development environments are becoming more and more complex, consists of different services and run often in containers.
Although similar great approaches exist (e.g. [Docker Compose](https://docs.docker.com/compose/), [Skaffold](https://skaffold.dev/)), they don't fit exactly our use case.
They rely highly on the CLI and standalone definitions written in [YAML](https://yaml.org/).
Since this library will be used in [DIME](https://gitlab.com/scce/dime), we need an implementation that is directly available through Java without making a detour via the CLI.

**Principles**.
This is not a general purpose container orchestration library.
It contains only a subset of possible container settings that are needed by DIME.
The API of the environment configuration is strongly influenced by the [compose file](https://docs.docker.com/compose/compose-file/) of Docker Compose.

**Implementation**.
The library is written in Java SE 8 (LTS).
Its interface is currently implemented against the [Docker](https://docs.docker.com/) daemon, but a future migration to a different container engine is not ruled out.
It uses the [Java API client for Docker](https://github.com/docker-java/docker-java). 

## Example

```java

import info.scce.gantry.Container;
import info.scce.gantry.Environment;
import info.scce.gantry.GantryException;
import info.scce.gantry.Project;
import info.scce.gantry.impl.DockerProject;

import java.io.File;
import java.util.Collections;

public final class Main {
    public static void main(String[] args) throws GantryException {
        // create project and add metadata
        Project project = new DockerProject(
                // root path of the project
                new File("/tmp/app"),
                // name of the project will be used
                // to prefix container names (e.g. app_nginx)
                "app"
        );
        // create immutable environment
        Environment environment = project.environment(
                // define container
                project.container(
                        // container is based on a public
                        // image that will be pulled
                        project.pulledImage("nginx:latest"),
                        // container name will be prefixed by
                        // the projects name (e.g. app_nginx)
                        project.containerName("nginx"),
                        // configure additional settings
                        project.containerConfig(
                                // List of entrypoint parts
                                Collections.emptyList(),
                                // List of command parts
                                Collections.emptyList(),
                                // List of environment variables
                                Collections.emptyList(),
                                // List of links to other containers
                                Collections.emptyList(),
                                // List of port bindings,
                                Collections.singletonList(
                                        project.portSpec("127.0.0.1:8080", "80")
                                ),
                                // List of host directory bindings,
                                Collections.emptyList(),
                                // List of provided volumes,
                                Collections.emptyList()
                        )
                )
                // add more container here...
        );
        // use the environment to query a container by its name
        Container nginx = environment.container("nginx");
        // prepare the related image (pull or build it)
        nginx.prepareImage();
        // start the actual container
        nginx.start();
        // open http://127.0.0.1:8080 in your browser
    }
}
```

## Maven

```xml
<dependency>
    <groupId>info.scce</groupId>
    <artifactId>gantry</artifactId>
    <version>0.3.0</version>
</dependency>
```

## Icon

<div>Icons made by <a href="https://www.flaticon.com/authors/nhor-phai" title="Nhor Phai">Nhor Phai</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

### Flaticon License

Free for personal and commercial purpose with attribution.
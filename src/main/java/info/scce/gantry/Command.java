package info.scce.gantry;

/**
 * Command.
 *
 * <p>Represents a command for the container to run.</p>
 *
 * @see <a href="https://docs.docker.com/compose/compose-file/compose-file-v3/#command">Docker Docs</a>
 */
public interface Command {

    /**
     * Returns the serialized version of the command.
     *
     * @return Serialized version of the command
     */
    String serialized();
}

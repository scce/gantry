package info.scce.gantry;

/**
 * Container.
 *
 * <p>Represents a container which can be started and stopped. Before
 * starting a container, the underlying image can be prepared.</p>
 */
public interface Container {
    /**
     * Returns name of the container.
     *
     * @return The object representing the container name
     */
    ContainerName containerName();

    /**
     * Starts the container.
     *
     * @throws ContainerException If the starting of the container fails
     */
    void start() throws ContainerException;

    /**
     * Stops the container.
     *
     * @throws ContainerException If the stopping of the container fails
     */
    void stop() throws ContainerException;

    /**
     * Remove the container.
     */
    void remove() throws ContainerException;

    /**
     * Prepare the image which the container relates to.
     *
     * @throws ContainerException If the preparing of the image fails
     */
    void prepareImage() throws ContainerException;

    /**
     * Remove all volumes of this container.
     *
     * @throws ContainerException If one of the volumes can't be removed
     */
    void removeProvidedVolumes() throws ContainerException;

    /**
     * Returns current status of the container.
     *
     * @return The object representing the container status
     * @throws ContainerException If the creating of the container status fails
     */
    ContainerStatus containerStatus() throws ContainerException;

    /**
     * ContainerException.
     *
     * <p>Represents a failure during a container operation.</p>
     */
    final class ContainerException extends GantryException {
        public ContainerException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}

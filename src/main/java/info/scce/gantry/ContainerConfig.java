package info.scce.gantry;

import com.github.dockerjava.api.model.Bind;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.Link;
import com.github.dockerjava.api.model.PortBinding;

import java.util.List;

/**
 * ContainerConfig.
 *
 * <p>Represents the configuration of a container. Not all possible
 * settings of a container are present.</p>
 */
public interface ContainerConfig {
    /**
     * @return List of serialized entrypoints
     */
    List<String> entrypoints();

    /**
     * @return List of serialized commands
     */
    List<String> cmds();
    /**
     * @return List of serialized environment variables
     */
    List<String> env();

    /**
     * @return List of volume bindings
     */
    List<Bind> binds();

    /**
     * @return List of port bindings
     */
    List<PortBinding> portBindings();

    /**
     * @return List of links between containers
     */
    List<Link> links();

    /**
     * Remove all volumes of this container config.
     *
     * @throws ProvidedVolume.ProvidedVolumeException If one of the volumes can't be removed
     */
    void removeProvidedVolumes() throws ProvidedVolume.ProvidedVolumeException;

    /**
     * @return List of exposed ports
     */
    List<ExposedPort> exposedPorts();
}

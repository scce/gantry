package info.scce.gantry;

/**
 * ContainerName.
 *
 * <p>Represents the name of a container. It can be used to
 * configure a container at creation time.</p>
 */
public interface ContainerName {
    /**
     * Returns the actual container name.
     *
     * @return The actual container name
     */
    String name();
}

package info.scce.gantry;

/**
 * ContainerStatus.
 *
 * <p>Represents the status of a container. The actual status
 * is indicated by {@link ContainerStatusEnum}.</p>
 */
public interface ContainerStatus {
    /**
     * Returns current status of the container.
     *
     * @return The enum representing the container status
     * @throws ContainerStatusException If the status of the container can't be determined
     */
    ContainerStatusEnum status() throws ContainerStatusException;


    /**
     * ContainerStatusException.
     *
     * <p>Represents a failure during a container status operation.</p>
     */
    final class ContainerStatusException extends GantryException {
        public ContainerStatusException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}

package info.scce.gantry;

public enum ContainerStatusEnum {
    RUNNING,
    STOPPED,
    NOT_EXISTING,
    UNKNOWN
}

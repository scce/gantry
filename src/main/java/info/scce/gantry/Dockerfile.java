package info.scce.gantry;

import java.io.File;

/**
 * Dockerfile.
 *
 * <p>Represents a Dockerfile and it's base directory. It can be
 * used to create {@link Image}s.</p>
 *
 * @see <a href="https://docs.docker.com/engine/reference/builder/">Docker Docs</a>
 */
public interface Dockerfile {
    /**
     * Returns a File representing the Dockerfile.
     *
     * @return File representing the Dockerfile
     */
    File dockerfile();

    /**
     * Returns a File representing the base directory of the Dockerfile.
     *
     * @return File representing the base directory of the Dockerfile
     */
    File baseDirectory();
}

package info.scce.gantry;

/**
 * Entrypoint.
 *
 * <p>Represents an entrypoint for the container to run.</p>
 *
 * @see <a href="https://docs.docker.com/compose/compose-file/compose-file-v3/#entrypoint">Docker Docs</a>
 */
public interface Entrypoint {

    /**
     * Returns the serialized version of the entrypoint.
     *
     * @return Serialized version of the entrypoint
     */
    String serialized();
}

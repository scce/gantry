package info.scce.gantry;

/**
 * Environment.
 *
 * <p>Represents the environment which contains all
 * configured containers.</p>
 */
public interface Environment {
    /**
     * Queries and returns the container by name.
     *
     * @param name Name used to query the container.
     * @return Container found by the name parameter.
     */
    Container container(String name);

    /**
     * Checks the preconditions of the environment.
     *
     * @throws EnvironmentException If the runtime dependencies of the environment fail the check
     */
    void checkPreconditions() throws EnvironmentException;

    /**
     * Creates a new environment with the passed container.
     *
     * @param container Container that will be added to the new environment
     * @return Environment that has been created with the passed container
     * @throws EnvironmentException If the name of the passed container is already present
     */
    Environment withContainer(Container container) throws EnvironmentException;

    /**
     * Creates a new environment without the container represented by the passed name.
     *
     * @param name Name that will used to remove the related container
     * @return Environment that has been created without container represented by the passed name
     * @throws EnvironmentException If no container represented by the passed name is present
     */
    Environment withoutContainer(String name) throws EnvironmentException;

    /**
     * EnvironmentException.
     *
     * <p>Represents a failure during an environment operation.</p>
     */
    final class EnvironmentException extends GantryException {
        public EnvironmentException(String message) {
            super(message);
        }

        public EnvironmentException(String message, Throwable cause) {
            super(message, cause);
        }

    }
}

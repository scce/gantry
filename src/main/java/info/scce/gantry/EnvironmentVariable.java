package info.scce.gantry;

/**
 * EnvironmentVariable.
 *
 * <p>Represents an environment variable which can be injected
 * to a container at run-time.</p>
 *
 * @see <a href="https://docs.docker.com/engine/reference/run/#env-environment-variables">Docker Docs</a>
 */
public interface EnvironmentVariable {

    /**
     * Returns the serialized version of the environment variable.
     *
     * @return Serialized version in the format key=value (e.g. user=admin) of the environment variable
     */
    String serialized();
}

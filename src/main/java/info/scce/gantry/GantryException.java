package info.scce.gantry;

/**
 * GantryException.
 *
 * <p>Abstract exception which is extended by all exceptions that
 * have to be caught by the user.</p>
 */
public abstract class GantryException extends Exception {

    public GantryException() {
        super();
    }

    public GantryException(String message) {
        super(message);
    }

    public GantryException(String message, Throwable cause) {
        super(message, cause);
    }

}

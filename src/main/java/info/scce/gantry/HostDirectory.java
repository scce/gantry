package info.scce.gantry;

/**
 * VolumeBinding.
 *
 * <p>Represents a volume binding. It
 * can be used to configure a container.</p>
 *
 * @see <a href="https://docs.docker.com/engine/reference/run/#volume-shared-filesystems">Docker Docs</a>
 */
public interface HostDirectory {
    /**
     * Returns the serialized volume binding.
     *
     * @return The serialized version of the volume binding in the format hostPath:containerPath (e.g. /tmp/nginx.conf:/etc/nginx/nginx.conf)
     */
    String serialized();
}

package info.scce.gantry;

/**
 * Image.
 *
 * <p>Represents an image which can be used to
 * create a {@link Container}. An image is either
 * build or pulled.</p>
 */
public interface Image {

    /**
     * Returns name of the image.
     *
     * @return The object representing the image name
     */
    ImageName imageName();

    /**
     * Prepare (pull or build) the image.
     *
     * @throws ImageException If the image preparing fails
     */
    void prepare() throws ImageException;

    /**
     * Removes the image.
     *
     * @throws ImageException If the image removing fails
     */
    void remove() throws ImageException;

    /**
     * ImageException.
     *
     * <p>Represents a failure during an image operation.</p>
     */
    final class ImageException extends GantryException {
        public ImageException() {
            super();
        }

        public ImageException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}

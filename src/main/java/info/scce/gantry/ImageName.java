package info.scce.gantry;

/**
 * ImageName.
 *
 * <p>Represents the name of a image. It can be used to
 * either build or pull an image.</p>
 */
public interface ImageName {
    /**
     * Returns the actual image name.
     *
     * @return The actual image name
     */
    String name();
}

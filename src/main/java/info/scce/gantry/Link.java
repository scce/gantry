package info.scce.gantry;

/**
 * Link.
 *
 * <p>Represents a link to a container. It
 * can be used to configure a container.</p>
 *
 * @see <a href="https://docs.docker.com/engine/reference/run/#expose-incoming-ports">Docker Docs</a>
 */
public interface Link {
    /**
     * Returns the name.
     *
     * @return The name in the format projectName_containerName (e.g. app_nginx)
     */
    String name();

    /**
     * Returns the alias.
     *
     * @return The alias of the link which will be available as a hostname in the targeted container.
     */
    String alias();
}

package info.scce.gantry;

/**
 * PortSpec.
 *
 * <p>Represents a port specification. It
 * can be used to configure a container.</p>
 *
 * @see <a href="https://docs.docker.com/engine/reference/run/#expose-incoming-ports">Docker Docs</a>
 */
public interface PortSpec {
    /**
     * Returns the serialized port specification.
     *
     * @return The serialized port specification in the format host:container (e.g. 80:8080)
     */
    String serialized();

    /**
     * todo
     * @return
     */
    String exposedPort();

}

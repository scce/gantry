package info.scce.gantry;

import java.util.List;

/**
 * Project.
 *
 * <p>Represents a project which can be used create project-specific
 * entities.</p>
 */
public interface Project {

    /**
     * Creates a project-specific container name.
     *
     * @param name Name of the container
     * @return Created container name
     */
    ContainerName containerName(String name);

    /**
     * Creates a project-specific image.
     * It expects the Dockerfile located in the root of the project folder.
     * <p>
     * /project
     * └── Dockerfile
     *
     * @param name Name of the image
     * @return Created image
     */
    Image builtImage(String name);

    /**
     * Creates a project-specific image with a custom {@code dockerfile}.
     * It expects the {@code dockerfile} located in the root of the project folder.
     * <p>
     * /project
     * └── {@code dockerfile}
     *
     * @param name       Name of the image
     * @param dockerfile Filename of the Dockerfile
     * @return Created image
     */
    Image builtImage(String name, String dockerfile);

    /**
     * Creates a project-specific image.
     * It expects the Dockerfile located in the subfolder {@code name} of the project folder.
     * <p>
     * /project
     * └── {@code name}
     * └── Dockerfile
     *
     * @param name Name of the image and subfolder containing the Dockerfile
     * @return Created image
     */
    Image builtImageSubfolder(String name);

    /**
     * Creates a project-specific image with a custom Dockerfile.
     * It expects the Dockerfile located in the subfolder {@code name} of the project folder.
     * <p>
     * /project
     * └── {@code name}
     * └── {@code dockerfile}
     *
     * @param name       Name of the image and subfolder containing the {@code dockerfile}
     * @param dockerfile Filename of the Dockerfile
     * @return Created image
     */
    Image builtImageSubfolder(String name, String dockerfile);

    /**
     * Creates a project-specific binding for a host directory (i.e. bind mounts).
     *
     * @param hostPath      Path to the file/directory on the host
     * @param containerPath Path to the file/directory on the container
     * @return Created host directory
     * @see <a href="https://docs.docker.com/storage/bind-mounts/">https://docs.docker.com/storage/bind-mounts/</a>
     */
    HostDirectory hostDirectory(String hostPath, String containerPath);

    /**
     * Creates a project-specific binding for a provided volume (i.e. named volume).
     *
     * @param name          Name used to identify the volume
     * @param containerPath Path to the file/directory on the container
     * @return Created provided volume
     * @see <a href="https://docs.docker.com/storage/volumes/">https://docs.docker.com/storage/volumes/</a>
     */
    ProvidedVolume providedVolume(String name, String containerPath);

    /**
     * Creates a project-specific image.
     *
     * @param name Name of the image
     * @return Created image
     */
    Image pulledImage(String name);

    /**
     * Creates a project-specific container.
     *
     * @param image           Will be used to create the container.
     * @param containerName   Will be used to name the container.
     * @param containerConfig Will be used to configure the container.
     * @return Created container.
     */
    Container container(
            Image image,
            ContainerName containerName,
            ContainerConfig containerConfig
    );

    /**
     * Creates a project-specific environment which contains all passed containers.
     *
     * @param containers Will be used to create the environment.
     * @return Created environment.
     */
    Environment environment(Container... containers);

    /**
     * Creates a project-specific list of entrypoints.
     *
     * @param entrypoints Entrypoint(s) for the container to run
     * @return Created list of entrypoints
     */
    List<Entrypoint> entrypoint(String... entrypoints);

    /**
     * Creates a project-specific list of commands.
     *
     * @param commands Command(s) for the container to run
     * @return Created list of commands
     */
    List<Command> command(String... commands);

    /**
     * Creates a project-specific port specification.
     *
     * @param host      Port that will be open on the host
     * @param container Port of the container that will be exposed
     * @return Created port specification
     */
    PortSpec portSpec(String host, String container);

    /**
     * Creates a project-specific environment variable.
     *
     * @param key   Key of the environment variable (e.g. username)
     * @param value Value of the environment variable (e.g. admin)
     * @return Created environment variable
     */
    EnvironmentVariable environmentVariable(String key, String value);

    /**
     * Creates a project-specific environment variable.
     *
     * @param name Name of the container to be linked
     * @return Created link
     */
    Link link(String name);

    /**
     * Creates a project-specific container config.
     *
     * @param entrypointList          List of serialized entrypoints
     * @param commandList             List of serialized commands
     * @param environmentVariableList List of serialized environment variables
     * @param linkList                List of volume bindings
     * @param portSpecList            List of port bindings
     * @param hostDirectoryList       List of links between containers
     * @return Created container config
     */
    ContainerConfig containerConfig(
            List<Entrypoint> entrypointList,
            List<Command> commandList,
            List<EnvironmentVariable> environmentVariableList,
            List<Link> linkList,
            List<PortSpec> portSpecList,
            List<HostDirectory> hostDirectoryList,
            List<ProvidedVolume> providedVolumeList
    );

}

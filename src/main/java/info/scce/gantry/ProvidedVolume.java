package info.scce.gantry;

/**
 * VolumeBinding.
 *
 * <p>Represents a volume binding. It
 * can be used to configure a container.</p>
 *
 * @see <a href="https://docs.docker.com/engine/reference/run/#volume-shared-filesystems">Docker Docs</a>
 */
public interface ProvidedVolume {

    /**
     * Returns the name.
     *
     * @return Name used to identify the volume
     */
    String name();

    /**
     * Returns the container path.
     *
     * @return Path to the file/directory on the container
     */
    String containerPath();

    /**
     * Removes the provided volume.
     *
     * @throws ProvidedVolumeException If the provided volume removing fails
     */
    void remove() throws ProvidedVolumeException;


    /**
     * ProvidedVolumeException.
     *
     * <p>Represents a failure during a provided volume operation.</p>
     */
    final class ProvidedVolumeException extends GantryException {
        public ProvidedVolumeException() {
            super();
        }

        public ProvidedVolumeException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}

package info.scce.gantry.fake;

import info.scce.gantry.Command;

public class FakeCommand implements Command {
    private final String serialized;

    public FakeCommand(String serialized) {
        this.serialized = serialized;
    }

    public static FakeCommand simple() {
        return new FakeCommand("-c foo=bar");
    }

    @Override
    public String serialized() {
        return serialized;
    }
}

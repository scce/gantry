package info.scce.gantry.fake;

import info.scce.gantry.Container;
import info.scce.gantry.ContainerName;
import info.scce.gantry.ContainerStatus;

/**
 * Fake{@link Container}.
 */
public final class FakeContainer implements Container {
    private final ContainerStatus containerStatus;
    private final ContainerName containerName;
    private boolean startHasBeenCalled = false;
    private boolean prepareImageHasBeenCalled = false;
    private boolean removeProvidedVolumesHasBeenCalled = false;
    private boolean stopHasBeenCalled = false;
    private boolean removeHasBeenCalled = false;

    public FakeContainer(
            ContainerStatus containerStatus,
            ContainerName containerName
    ) {
        this.containerStatus = containerStatus;
        this.containerName = containerName;
    }

    @Override
    public ContainerName containerName() {
        return containerName;
    }

    @Override
    public void start() {
        startHasBeenCalled = true;
    }

    @Override
    public void stop() {
        stopHasBeenCalled = true;
    }

    @Override
    public void remove() {
        removeHasBeenCalled = true;
    }

    @Override
    public void prepareImage() {
        prepareImageHasBeenCalled = true;
    }

    @Override
    public void removeProvidedVolumes() {
        removeProvidedVolumesHasBeenCalled = true;
    }

    @Override
    public ContainerStatus containerStatus() {
        return containerStatus;
    }

    public boolean startHasBeenCalled() {
        return startHasBeenCalled;
    }

    public boolean prepareImageHasBeenCalled() {
        return prepareImageHasBeenCalled;
    }

    public boolean stopHasBeenCalled() {
        return stopHasBeenCalled;
    }

    public boolean removeHasBeenCalled() {
        return removeHasBeenCalled;
    }

    public boolean removeProvidedVolumesHasBeenCalled(){
        return removeProvidedVolumesHasBeenCalled;
    }
}

package info.scce.gantry.fake;

import com.github.dockerjava.api.model.Bind;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.Link;
import com.github.dockerjava.api.model.PortBinding;
import info.scce.gantry.ContainerConfig;
import info.scce.gantry.ProvidedVolume;

import java.util.Collections;
import java.util.List;

/**
 * Fake{@link ContainerConfig}.
 */
public final class FakeContainerConfig implements ContainerConfig {
    private final List<String> entrypointList;
    private final List<String> cmdList;
    private final List<Bind> bindList;
    private final List<String> envList;
    private final List<PortBinding> portBindingList;
    private final List<ExposedPort> exposedPortList;
    private final List<Link> linkList;
    private final boolean failRemoveProvidedVolumes;
    private boolean removeProvidedVolumesHasBeenCalled = false;

    public FakeContainerConfig(
            List<String> entrypointList,
            List<String> cmdList,
            List<Bind> bindList,
            List<String> envList,
            List<PortBinding> portBindingList,
            List<ExposedPort> exposedPortList,
            List<Link> linkList,
            boolean failRemoveProvidedVolumes
    ) {
        this.entrypointList = entrypointList;
        this.cmdList = cmdList;
        this.bindList = bindList;
        this.envList = envList;
        this.portBindingList = portBindingList;
        this.exposedPortList = exposedPortList;
        this.linkList = linkList;
        this.failRemoveProvidedVolumes = failRemoveProvidedVolumes;
    }

    public static ContainerConfig simple() {
        return new FakeContainerConfig(
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                false
        );
    }

    public static FakeContainerConfig fail() {
        return new FakeContainerConfig(
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                true
        );
    }

    @Override
    public List<String> entrypoints() {
        return entrypointList;
    }

    @Override
    public List<String> cmds() {
        return cmdList;
    }

    @Override
    public List<String> env() {
        return envList;
    }

    @Override
    public List<Bind> binds() {
        return bindList;
    }

    @Override
    public List<PortBinding> portBindings() {
        return portBindingList;
    }

    @Override
    public List<ExposedPort> exposedPorts() {
        return exposedPortList;
    }

    @Override
    public List<Link> links() {
        return linkList;
    }

    @Override
    public void removeProvidedVolumes() throws ProvidedVolume.ProvidedVolumeException {
        removeProvidedVolumesHasBeenCalled = true;
        if (failRemoveProvidedVolumes) {
            throw new ProvidedVolume.ProvidedVolumeException();
        }
    }

    public boolean removeProvidedVolumesHasBeenCalled() {
        return removeProvidedVolumesHasBeenCalled;
    }

}

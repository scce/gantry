package info.scce.gantry.fake;

import info.scce.gantry.ContainerName;

/**
 * Fake{@link ContainerName}.
 */
public final class FakeContainerName implements ContainerName {
    private final String name;

    public FakeContainerName(String name) {
        this.name = name;
    }

    public static FakeContainerName simple() {
        return new FakeContainerName("name");
    }

    @Override
    public String name() {
        return name;
    }
}

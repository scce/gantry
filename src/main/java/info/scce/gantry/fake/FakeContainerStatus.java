package info.scce.gantry.fake;

import info.scce.gantry.ContainerStatus;
import info.scce.gantry.ContainerStatusEnum;

/**
 * Fake{@link ContainerStatus}.
 */
public final class FakeContainerStatus implements ContainerStatus {
    private final ContainerStatusEnum containerStatusEnum;

    public FakeContainerStatus(ContainerStatusEnum containerStatusEnum) {
        this.containerStatusEnum = containerStatusEnum;
    }

    public static FakeContainerStatus simple() {
        return new FakeContainerStatus(ContainerStatusEnum.RUNNING);
    }

    @Override
    public ContainerStatusEnum status() {
        return containerStatusEnum;
    }
}

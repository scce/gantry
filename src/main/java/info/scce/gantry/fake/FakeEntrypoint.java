package info.scce.gantry.fake;

import info.scce.gantry.Entrypoint;

public class FakeEntrypoint implements Entrypoint {
    private final String serialized;

    public FakeEntrypoint(String serialized) {
        this.serialized = serialized;
    }

    public static FakeEntrypoint simple() {
        return new FakeEntrypoint("postgres");
    }

    @Override
    public String serialized() {
        return serialized;
    }
}

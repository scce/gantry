package info.scce.gantry.fake;

import info.scce.gantry.EnvironmentVariable;

/**
 * Fake{@link EnvironmentVariable}.
 */
public final class FakeEnvironmentVariable implements EnvironmentVariable {
    private final String serialized;

    public FakeEnvironmentVariable(String serialized) {
        this.serialized = serialized;
    }

    public static FakeEnvironmentVariable simple() {
        return new FakeEnvironmentVariable("KEY=VALUE");
    }

    @Override
    public String serialized() {
        return serialized;
    }
}

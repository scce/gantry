package info.scce.gantry.fake;

import info.scce.gantry.HostDirectory;

/**
 * Fake{@link HostDirectory}.
 */
public final class FakeHostDirectory implements HostDirectory {
    private final String serialized;

    public FakeHostDirectory(String serialized) {
        this.serialized = serialized;
    }

    public static FakeHostDirectory simple() {
        return new FakeHostDirectory("/tmp/foo:/tmp/bar");
    }

    @Override
    public String serialized() {
        return serialized;
    }
}

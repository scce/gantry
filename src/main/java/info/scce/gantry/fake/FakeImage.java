package info.scce.gantry.fake;

import info.scce.gantry.Image;
import info.scce.gantry.ImageName;

/**
 * Fake{@link Image}.
 */
public final class FakeImage implements Image {
    private final ImageName imageName;
    private final boolean failPrepare;
    private final boolean failRemove;
    private boolean prepareHasBeenCalled = false;
    private boolean removeHasBeenCalled = false;

    public FakeImage(
            ImageName imageName,
            boolean failPrepare,
            boolean failRemove
    ) {
        this.imageName = imageName;
        this.failPrepare = failPrepare;
        this.failRemove = failRemove;
    }

    public static FakeImage simple() {
        return new FakeImage(FakeImageName.simple(), false, false);
    }

    public static FakeImage failed() {
        return new FakeImage(FakeImageName.simple(), true, true);
    }

    @Override
    public ImageName imageName() {
        return imageName;
    }

    @Override
    public void prepare() throws ImageException {
        prepareHasBeenCalled = true;
        if (failPrepare) {
            throw new ImageException();
        }
    }

    @Override
    public void remove() throws ImageException {
        removeHasBeenCalled = true;
        if (failRemove) {
            throw new ImageException();
        }
    }

    public boolean prepareHasBeenCalled() {
        return prepareHasBeenCalled;
    }

    public boolean removeHasBeenCalled() {
        return removeHasBeenCalled;
    }
}

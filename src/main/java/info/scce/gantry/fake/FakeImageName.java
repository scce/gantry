package info.scce.gantry.fake;

import info.scce.gantry.ImageName;

/**
 * Fake{@link ImageName}.
 */
public final class FakeImageName implements ImageName {
    private final String name;

    public FakeImageName(String name) {
        this.name = name;
    }

    public static ImageName simple() {
        return new FakeImageName("simple");
    }

    @Override
    public String name() {
        return name;
    }
}

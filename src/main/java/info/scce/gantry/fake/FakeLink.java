package info.scce.gantry.fake;

import info.scce.gantry.Link;

/**
 * Fake{@link Link}.
 */
public final class FakeLink implements Link {
    private final String name;
    private final String alias;

    public FakeLink(String name, String alias) {
        this.name = name;
        this.alias = alias;
    }

    public static FakeLink simple() {
        return new FakeLink("name", "alias");
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String alias() {
        return alias;
    }
}

package info.scce.gantry.fake;

import info.scce.gantry.PortSpec;

/**
 * Fake{@link PortSpec}.
 */
public class FakePortSpec implements PortSpec {
    private final String serialized;
    private final String exposedPort;

    public FakePortSpec(String serialized, String exposedPort) {
        this.serialized = serialized;
        this.exposedPort = exposedPort;
    }

    public static FakePortSpec simple() {
        return new FakePortSpec("8080:8080", "8080");
    }

    @Override
    public String serialized() {
        return serialized;
    }

    @Override
    public String exposedPort() {
        return exposedPort;
    }
}

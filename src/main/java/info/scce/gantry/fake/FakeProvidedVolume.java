package info.scce.gantry.fake;

import info.scce.gantry.ProvidedVolume;

/**
 * Fake{@link ProvidedVolume}.
 */
public final class FakeProvidedVolume implements ProvidedVolume {
    private final String name;
    private final String containerPath;
    private boolean removeHasBeenCalled = false;

    public FakeProvidedVolume(String name, String containerPath) {
        this.name = name;
        this.containerPath = containerPath;
    }

    public static FakeProvidedVolume simple() {
        return new FakeProvidedVolume("foo", "/tmp/bar");
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String containerPath() {
        return containerPath;
    }

    @Override
    public void remove() {
        removeHasBeenCalled = true;
    }

    public boolean removeHasBeenCalled() {
        return removeHasBeenCalled;
    }

}

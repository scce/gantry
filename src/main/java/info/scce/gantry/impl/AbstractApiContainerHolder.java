package info.scce.gantry.impl;

import info.scce.gantry.impl.api.ApiContainer;

abstract class AbstractApiContainerHolder {
    protected final ApiContainer apiContainer;

    protected AbstractApiContainerHolder(
            ApiContainer apiContainer
    ) {
        this.apiContainer = apiContainer;
    }
}

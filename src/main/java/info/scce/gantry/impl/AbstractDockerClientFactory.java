package info.scce.gantry.impl;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.DockerClientImpl;
import com.github.dockerjava.httpclient5.ApacheDockerHttpClient;

abstract class AbstractDockerClientFactory implements DockerClientFactory {

    protected abstract DockerClientConfig dockerClientConfig();

    @Override
    public DockerClient build() {
        DockerClientConfig dockerClientConfig = dockerClientConfig();
        return DockerClientImpl.getInstance(
                dockerClientConfig,
                new ApacheDockerHttpClient
                        .Builder()
                        .dockerHost(dockerClientConfig.getDockerHost())
                        .sslConfig(dockerClientConfig.getSSLConfig())
                        .build()
        );
    }

}

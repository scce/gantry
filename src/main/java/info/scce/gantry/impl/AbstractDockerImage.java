package info.scce.gantry.impl;

import info.scce.gantry.Image;
import info.scce.gantry.Image.ImageException;
import info.scce.gantry.ImageName;
import info.scce.gantry.impl.api.ApiException;
import info.scce.gantry.impl.api.ApiImage;

abstract class AbstractDockerImage {
    protected final ImageName name;
    protected final ApiImage apiImage;

    public AbstractDockerImage(
            ApiImage apiImage,
            ImageName name
    ) {
        this.apiImage = apiImage;
        this.name = name;
    }

    public ImageName imageName() {
        return name;
    }

    public void remove() throws ImageException {
        try {
            apiImage.remove();
        } catch (ApiException e) {
            throw new Image.ImageException(
                    String.format(
                            "We failed pulling the image \"%s\"",
                            imageName().name()
                    ),
                    e
            );
        }
    }
}

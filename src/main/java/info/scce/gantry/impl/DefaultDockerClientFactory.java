package info.scce.gantry.impl;

import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientConfig;

/**
 * DefaultDockerClientFactory.
 *
 * <p>Factory for DockerClients with default configuration for Linux and Mac.</p>
 */
public final class DefaultDockerClientFactory extends AbstractDockerClientFactory {

    protected DockerClientConfig dockerClientConfig() {
        return DefaultDockerClientConfig
                .createDefaultConfigBuilder()
                .build();
    }

}

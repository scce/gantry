package info.scce.gantry.impl;

import info.scce.gantry.Image;
import info.scce.gantry.ImageName;
import info.scce.gantry.impl.api.ApiBuildImage;
import info.scce.gantry.impl.api.ApiException;

/**
 * DockerBuilt{@link Image}.
 */
public final class DockerBuiltImage extends AbstractDockerImage implements Image {
    private final ApiBuildImage apiBuildImage;

    public DockerBuiltImage(
            ApiBuildImage apiBuildImage,
            ImageName name
    ) {
        super(apiBuildImage, name);
        this.apiBuildImage = apiBuildImage;
    }

    @Override
    public void prepare() throws ImageException {
        try {
            apiBuildImage.build();
        } catch (ApiException e) {
            throw new ImageException(
                    String.format(
                            "We failed building the image \"%s\"",
                            imageName().name()
                    ),
                    e
            );
        }
    }

}

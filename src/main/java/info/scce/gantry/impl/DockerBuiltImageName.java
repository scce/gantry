package info.scce.gantry.impl;

import info.scce.gantry.ImageName;

/**
 * DockerBuilt{@link ImageName}.
 */
public final class DockerBuiltImageName implements ImageName {
    private final String projectName;
    private final String imageName;

    public DockerBuiltImageName(String projectName, String imageName) {
        this.projectName = projectName;
        this.imageName = imageName;
    }

    @Override
    public String name() {
        return projectName + "_" + imageName;
    }
}

package info.scce.gantry.impl;

import com.github.dockerjava.api.DockerClient;

/**
 * DockerClientFactory.
 *
 * <p>Factory to create instances of {@link DockerClient}</p>
 */
public interface DockerClientFactory {

    /**
     * Returns the new {@link DockerClient} instance.
     *
     * @return The new {@link DockerClient} instance
     */
    DockerClient build();
}

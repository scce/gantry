package info.scce.gantry.impl;

import info.scce.gantry.Command;

/**
 * Docker{@link Command}.
 */
public final class DockerCommand implements Command {
    private final String command;

    public DockerCommand(String command) {
        this.command = command;
    }

    @Override
    public String serialized() {
        return command;
    }
}

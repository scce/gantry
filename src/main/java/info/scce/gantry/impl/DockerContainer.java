package info.scce.gantry.impl;

import info.scce.gantry.*;
import info.scce.gantry.impl.api.ApiContainer;
import info.scce.gantry.impl.api.ApiContainerList;
import info.scce.gantry.impl.api.ApiException;

/**
 * Docker{@link Container}.
 */
public final class DockerContainer implements Container {
    private final ApiContainerList apiContainerList;
    private final Image image;
    private final ContainerName name;
    private final ContainerConfig containerConfig;

    public DockerContainer(
            ApiContainerList apiContainerList,
            Image image,
            ContainerName name,
            ContainerConfig containerConfig
    ) {
        this.apiContainerList = apiContainerList;
        this.image = image;
        this.name = name;
        this.containerConfig = containerConfig;
    }

    @Override
    public ContainerName containerName() {
        return this.name;
    }

    @Override
    public void start() throws ContainerException {
        try {
            ApiContainer apiContainer;
            if (apiContainerList.exists(name)) {
                apiContainer = apiContainerList.retrieve(name);
            } else {
                apiContainer = apiContainerList.create(image, name, containerConfig);
            }
            if (apiContainer.isStopped()) {
                apiContainer.start();
            }
        } catch (ApiException e) {
            throw new ContainerException(
                    String.format(
                            "We failed starting the container \"%s\"",
                            name.name()
                    ),
                    e
            );
        }
    }

    @Override
    public void stop() throws ContainerException {
        try {
            if (apiContainerList.exists(name)) {
                ApiContainer apiContainer = apiContainerList.retrieve(name);
                if (apiContainer.isRunning()) {
                    apiContainer.stop();
                }
            }
        } catch (ApiException e) {
            throw new ContainerException(
                    String.format(
                            "We failed stopping the container \"%s\"",
                            name.name()
                    ),
                    e
            );
        }
    }

    @Override
    public void remove() throws ContainerException {
        try {
            if (apiContainerList.exists(name)) {
                apiContainerList
                        .retrieve(name)
                        .remove();
            }
        } catch (ApiException e) {
            throw new ContainerException(
                    String.format(
                            "We failed removing the container \"%s\"",
                            name.name()
                    ),
                    e
            );
        }
    }

    @Override
    public void prepareImage() throws ContainerException {
        try {
            image.prepare();
        } catch (Image.ImageException e) {
            throw new ContainerException(
                    String.format(
                            "We failed preparing the image of container \"%s\"",
                            name.name()
                    ),
                    e
            );
        }
    }

    @Override
    public void removeProvidedVolumes() throws ContainerException {
        try {
            containerConfig.removeProvidedVolumes();
        } catch (ProvidedVolume.ProvidedVolumeException e) {
            throw new ContainerException(
                    String.format(
                            "We failed removing the volume of container \"%s\"",
                            name.name()
                    ),
                    e
            );
        }
    }

    @Override
    public ContainerStatus containerStatus() throws ContainerException {
        try {
            if (apiContainerList.exists(name)) {
                return new DockerContainerStatus(name, apiContainerList.retrieve(name));
            }
            return new DockerNotExistingContainerStatus();
        } catch (ApiException e) {
            throw new ContainerException(
                    String.format(
                            "We failed creating the status of container \"%s\"",
                            name.name()
                    ),
                    e
            );
        }
    }

}

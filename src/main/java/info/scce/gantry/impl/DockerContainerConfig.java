package info.scce.gantry.impl;

import com.github.dockerjava.api.model.Bind;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.PortBinding;
import com.github.dockerjava.api.model.Volume;
import info.scce.gantry.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Docker{@link ContainerConfig}.
 */
public final class DockerContainerConfig implements ContainerConfig {
    private final List<Command> commandList;
    private final List<Entrypoint> entrypointList;
    private final List<EnvironmentVariable> environmentVariableList;
    private final List<Link> linkList;
    private final List<PortSpec> portSpecList;
    private final List<HostDirectory> hostDirectoryList;
    private final List<ProvidedVolume> providedVolumeList;

    public DockerContainerConfig(
            List<Entrypoint> entrypointList,
            List<Command> commandList,
            List<EnvironmentVariable> environmentVariableList,
            List<Link> linkList,
            List<PortSpec> portSpecList,
            List<HostDirectory> hostDirectoryList,
            List<ProvidedVolume> providedVolumeList
    ) {
        this.entrypointList = entrypointList;
        this.commandList = commandList;
        this.environmentVariableList = environmentVariableList;
        this.linkList = linkList;
        this.portSpecList = portSpecList;
        this.hostDirectoryList = hostDirectoryList;
        this.providedVolumeList = providedVolumeList;
    }

    @Override
    public List<String> env() {
        return environmentVariableList
                .stream()
                .map(EnvironmentVariable::serialized)
                .collect(Collectors.toList());
    }

    @Override
    public List<PortBinding> portBindings() {
        return portSpecList
                .stream()
                .map(portSpec -> PortBinding.parse(portSpec.serialized()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Bind> binds() {
        return Stream
                .concat(
                        providedVolumeList.stream().map(providedVolume -> new Bind(providedVolume.name(), new Volume(providedVolume.containerPath()))),
                        hostDirectoryList.stream().map(hostDirectory -> Bind.parse(hostDirectory.serialized()))
                )
                .collect(Collectors.toList());
    }

    @Override
    public List<com.github.dockerjava.api.model.Link> links() {
        return linkList
                .stream()
                .map(link -> new com.github.dockerjava.api.model.Link(link.name(), link.alias()))
                .collect(Collectors.toList());
    }

    @Override
    public List<String> entrypoints() {
        return entrypointList
                .stream()
                .map(Entrypoint::serialized)
                .collect(Collectors.toList());
    }

    @Override
    public List<String> cmds() {
        return commandList
                .stream()
                .map(Command::serialized)
                .collect(Collectors.toList());
    }

    @Override
    public void removeProvidedVolumes() throws ProvidedVolume.ProvidedVolumeException {
        for (ProvidedVolume providedVolume : providedVolumeList) {
            providedVolume.remove();
        }
    }

    @Override
    public List<ExposedPort> exposedPorts() {
        return portSpecList
                .stream()
                .map(PortSpec::exposedPort)
                .map(ExposedPort::parse)
                .collect(Collectors.toList());
    }

}

package info.scce.gantry.impl;

import info.scce.gantry.ContainerName;

/**
 * Docker{@link ContainerName}.
 */
public final class DockerContainerName implements ContainerName {
    private final String projectName;
    private final String containerName;

    public DockerContainerName(String projectName, String containerName) {
        this.projectName = projectName;
        this.containerName = containerName;
    }

    @Override
    public String name() {
        return projectName + "_" + containerName;
    }
}

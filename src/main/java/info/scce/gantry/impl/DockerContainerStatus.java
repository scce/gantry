package info.scce.gantry.impl;

import info.scce.gantry.ContainerName;
import info.scce.gantry.ContainerStatus;
import info.scce.gantry.ContainerStatusEnum;
import info.scce.gantry.impl.api.ApiContainer;
import info.scce.gantry.impl.api.ApiException;

/**
 * Docker{@link ContainerStatus}.
 */
public final class DockerContainerStatus extends AbstractApiContainerHolder implements ContainerStatus {

    private final ContainerName name;

    public DockerContainerStatus(ContainerName name, ApiContainer apiContainer) {
        super(apiContainer);
        this.name = name;
    }

    public ContainerStatusEnum status() throws ContainerStatusException {
        try {
            if (apiContainer.isStopped()) {
                return ContainerStatusEnum.STOPPED;
            }
            if (apiContainer.isRunning()) {
                return ContainerStatusEnum.RUNNING;
            }
            return ContainerStatusEnum.UNKNOWN;
        } catch (ApiException e) {
            throw new ContainerStatusException(
                    String.format(
                            "We failed determining the status of container \"%s\"",
                            name.name()
                    ),
                    e
            );
        }
    }


}

package info.scce.gantry.impl;

import info.scce.gantry.Dockerfile;

import java.io.File;

/**
 * Docker{@link Dockerfile}.
 */
public final class DockerDockerfile implements Dockerfile {
    private final File baseDirectory;
    private final String dockerfile;

    public DockerDockerfile(File baseDirectory, String dockerfile) {
        this.baseDirectory = baseDirectory;
        this.dockerfile = dockerfile;
    }

    public DockerDockerfile(File baseDirectory, String serviceDirectory, String dockerfile) {
        this(new File(baseDirectory, serviceDirectory), dockerfile);
    }

    @Override
    public File dockerfile() {
        return new File(baseDirectory, dockerfile);
    }

    @Override
    public File baseDirectory() {
        return baseDirectory;
    }
}

package info.scce.gantry.impl;


import info.scce.gantry.Entrypoint;

/**
 * Docker{@link info.scce.gantry.Entrypoint}.
 */
public final class DockerEntrypoint implements Entrypoint {
    private final String command;

    public DockerEntrypoint(String command) {
        this.command = command;
    }

    @Override
    public String serialized() {
        return command;
    }
}

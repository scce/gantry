package info.scce.gantry.impl;

import com.github.dockerjava.api.DockerClient;
import info.scce.gantry.Container;
import info.scce.gantry.Environment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Docker{@link Environment}.
 */
public final class DockerEnvironment implements Environment {
    private final DockerClient dockerClient;
    private final String projectName;
    private final Map<String, Container> containerMap;

    public DockerEnvironment(DockerClient dockerClient, String projectName, List<Container> containerList) {
        this.dockerClient = dockerClient;
        this.projectName = projectName;
        this.containerMap = containerList
                .stream()
                .collect(Collectors.toMap(container -> container.containerName().name(), container -> container));
    }

    public DockerEnvironment(DockerClient dockerClient, String projectName, Container... containers) {
        this(dockerClient, projectName, Arrays.asList(containers));
    }

    @Override
    public Container container(String name) {
        return containerMap.get(new DockerContainerName(projectName, name).name());
    }

    @Override
    public Environment withContainer(Container container) throws EnvironmentException {
        if (containerMap.get(container.containerName().name()) != null) {
            throw new EnvironmentException(
                    String.format(
                            "We can't add the container \"%s\", since it is already present",
                            container.containerName().name()
                    )
            );
        }
        List<Container> containerList = new ArrayList<>(containerMap.values());
        containerList.add(container);
        return new DockerEnvironment(
                dockerClient,
                projectName,
                containerList
        );
    }

    @Override
    public Environment withoutContainer(String name) throws EnvironmentException {
        if (container(name) == null) {
            throw new EnvironmentException(
                    String.format(
                            "We can't remove the container \"%s\", since it is not present",
                            name
                    )
            );
        }
        List<Container> containerList = new ArrayList<>(containerMap.values());
        String containerName = new DockerContainerName(projectName, name).name();
        containerList.removeIf(container -> container.containerName().name().equals(containerName));
        return new DockerEnvironment(
                dockerClient,
                projectName,
                containerList
        );
    }

    @Override
    public void checkPreconditions() throws EnvironmentException {
        try {
            dockerClient
                    .authCmd()
                    .exec();
        } catch (UnsatisfiedLinkError e) {
            throw new EnvironmentException(
                    "We failed connecting to the docker daemon. Try to you use the proper DockerClientFactory for your operating system.",
                    e
            );
        } catch (RuntimeException e) {
            throw new EnvironmentException(
                    "We failed connecting to the docker daemon",
                    e
            );
        }
    }
}

package info.scce.gantry.impl;

import info.scce.gantry.EnvironmentVariable;

/**
 * Docker{@link EnvironmentVariable}.
 */
public final class DockerEnvironmentVariable implements EnvironmentVariable {
    private final String key;
    private final String value;

    public DockerEnvironmentVariable(String key, String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String serialized() {
        return key + "=" + value;
    }
}

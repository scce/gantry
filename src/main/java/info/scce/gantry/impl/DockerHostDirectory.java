package info.scce.gantry.impl;

import info.scce.gantry.HostDirectory;

import java.io.File;

/**
 * Docker{@link HostDirectory}.
 */
public final class DockerHostDirectory implements HostDirectory {
    private final String hostPath;
    private final String containerPath;

    public DockerHostDirectory(String hostPath, String containerPath) {
        this.hostPath = hostPath;
        this.containerPath = containerPath;
    }

    public DockerHostDirectory(File baseDirectory, String relativeHostPath, String containerPath) {
        this(new File(baseDirectory, relativeHostPath).getAbsolutePath(), containerPath);
    }

    public String serialized() {
        return hostPath + ":" + containerPath;
    }
}

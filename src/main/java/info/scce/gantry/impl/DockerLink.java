package info.scce.gantry.impl;

import info.scce.gantry.Link;

/**
 * Docker{@link Link}.
 */
public final class DockerLink implements Link {
    private final String projectName;
    private final String name;
    private final String alias;

    public DockerLink(String projectName, String name, String alias) {
        this.projectName = projectName;
        this.name = name;
        this.alias = alias;
    }

    @Override
    public String name() {
        return projectName + '_' + name;
    }

    @Override
    public String alias() {
        return alias;
    }


}

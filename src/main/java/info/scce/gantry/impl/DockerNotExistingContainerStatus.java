package info.scce.gantry.impl;

import info.scce.gantry.ContainerStatus;
import info.scce.gantry.ContainerStatusEnum;

/**
 * DockerNotExisting{@link ContainerStatus}.
 */
public final class DockerNotExistingContainerStatus implements ContainerStatus {
    public ContainerStatusEnum status() {
        return ContainerStatusEnum.NOT_EXISTING;
    }

}

package info.scce.gantry.impl;

import info.scce.gantry.PortSpec;

/**
 * Docker{@link PortSpec}.
 */
public final class DockerPortSpec implements PortSpec {
    private final String host;
    private final String container;

    public DockerPortSpec(String host, String container) {
        this.host = host;
        this.container = container;
    }

    @Override
    public String serialized() {
        return host + ":" + container;
    }

    @Override
    public String exposedPort() {
        return container;
    }
}

package info.scce.gantry.impl;

import com.github.dockerjava.api.DockerClient;
import info.scce.gantry.*;
import info.scce.gantry.impl.api.impl.DockerApiBuildImage;
import info.scce.gantry.impl.api.impl.DockerApiContainerList;
import info.scce.gantry.impl.api.impl.DockerApiPullImage;
import info.scce.gantry.impl.api.impl.DockerApiVolume;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Docker{@link Project}.
 */
public final class DockerProject implements Project {
    private final File baseDirectory;
    private final String name;
    private final DockerClient dockerClient;

    public DockerProject(File baseDirectory, String name, DockerClient dockerClient) {
        this.baseDirectory = baseDirectory;
        this.name = name;
        this.dockerClient = dockerClient;
    }

    public DockerProject(File baseDirectory, String name) {
        this(
                baseDirectory,
                name,
                new DefaultDockerClientFactory().build()
        );
    }

    @Override
    public HostDirectory hostDirectory(String hostPath, String containerPath) {
        return new DockerHostDirectory(
                baseDirectory,
                hostPath,
                containerPath
        );
    }

    @Override
    public ProvidedVolume providedVolume(String name, String containerPath) {
        // todo
        String volumeName = this.name + "_" + name;
        return new DockerProvidedVolume(
                new DockerApiVolume(dockerClient, volumeName),
                volumeName,
                containerPath
        );
    }

    @Override
    public ContainerName containerName(String name) {
        return new DockerContainerName(this.name, name);
    }

    @Override
    public Image pulledImage(String name) {
        DockerPulledImageName imageName = new DockerPulledImageName(name);
        return new DockerPulledImage(
                new DockerApiPullImage(
                        dockerClient,
                        imageName
                ),
                imageName
        );
    }

    @Override
    public Container container(
            Image image,
            ContainerName containerName,
            ContainerConfig containerConfig
    ) {
        return new DockerContainer(
                new DockerApiContainerList(dockerClient),
                image,
                containerName,
                containerConfig
        );
    }

    @Override
    public Environment environment(Container... containers) {
        return new DockerEnvironment(dockerClient, name, containers);
    }

    @Override
    public List<Entrypoint> entrypoint(String... entrypoints) {
        return Arrays.stream(entrypoints)
                .map(DockerEntrypoint::new)
                .collect(Collectors.toList());
    }

    @Override
    public List<Command> command(String... commands) {
        return Arrays.stream(commands)
                .map(DockerCommand::new)
                .collect(Collectors.toList());
    }

    @Override
    public PortSpec portSpec(String host, String container) {
        return new DockerPortSpec(
                host,
                container
        );
    }

    @Override
    public EnvironmentVariable environmentVariable(String key, String value) {
        return new DockerEnvironmentVariable(key, value);
    }

    @Override
    public Link link(String name) {
        return new DockerLink(this.name, name, name);
    }

    @Override
    public ContainerConfig containerConfig(
            List<Entrypoint> entrypointList,
            List<Command> commandList,
            List<EnvironmentVariable> environmentVariableList,
            List<Link> linkList,
            List<PortSpec> portSpecList,
            List<HostDirectory> hostDirectoryList,
            List<ProvidedVolume> providedVolumeList
    ) {
        return new DockerContainerConfig(
                entrypointList,
                commandList,
                environmentVariableList,
                linkList,
                portSpecList,
                hostDirectoryList,
                providedVolumeList
        );
    }

    @Override
    public Image builtImage(String name, String dockerfile) {
        DockerBuiltImageName imageName = new DockerBuiltImageName(this.name, name);
        return new DockerBuiltImage(
                new DockerApiBuildImage(
                        dockerClient,
                        imageName,
                        new DockerDockerfile(
                                baseDirectory,
                                dockerfile
                        )
                ),
                imageName
        );
    }

    @Override
    public Image builtImage(String name) {
        return builtImage(name, "Dockerfile");
    }

    @Override
    public Image builtImageSubfolder(String name, String dockerfile) {
        DockerBuiltImageName imageName = new DockerBuiltImageName(this.name, name);
        return new DockerBuiltImage(
                new DockerApiBuildImage(
                        dockerClient,
                        imageName,
                        new DockerDockerfile(
                                baseDirectory,
                                name,
                                dockerfile
                        )
                ),
                imageName
        );
    }

    @Override
    public Image builtImageSubfolder(String name) {
        return builtImageSubfolder(name, "Dockerfile");
    }

}

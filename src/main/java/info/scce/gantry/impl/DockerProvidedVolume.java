package info.scce.gantry.impl;

import info.scce.gantry.ProvidedVolume;
import info.scce.gantry.impl.api.ApiException;
import info.scce.gantry.impl.api.ApiVolume;

/**
 * Docker{@link ProvidedVolume}.
 */
public final class DockerProvidedVolume implements ProvidedVolume {
    private final ApiVolume apiVolume;
    private final String name;
    private final String containerPath;

    public DockerProvidedVolume(
            ApiVolume apiVolume,
            String name,
            String containerPath
    ) {
        this.apiVolume = apiVolume;
        this.name = name;
        this.containerPath = containerPath;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String containerPath() {
        return containerPath;
    }

    @Override
    public void remove() throws ProvidedVolumeException {
        try {
            if (apiVolume.exists()) {
                apiVolume.remove();
            }
        } catch (ApiException e) {
            throw new ProvidedVolumeException(
                    String.format(
                            "We failed removing the provided volume \"%s\"",
                            name
                    ),
                    e
            );
        }
    }
}

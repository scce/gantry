package info.scce.gantry.impl;

import info.scce.gantry.Image;
import info.scce.gantry.ImageName;
import info.scce.gantry.impl.api.ApiException;
import info.scce.gantry.impl.api.ApiPullImage;

/**
 * DockerPulled{@link Image}.
 */
public final class DockerPulledImage extends AbstractDockerImage implements Image {
    private final ApiPullImage apiPullImage;

    public DockerPulledImage(
            ApiPullImage apiPullImage,
            ImageName name
    ) {
        super(apiPullImage, name);
        this.apiPullImage = apiPullImage;
    }

    @Override
    public void prepare() throws ImageException {
        try {
            apiPullImage.pull();
        } catch (ApiException e) {
            throw new ImageException(
                    String.format(
                            "We failed pulling the image \"%s\"",
                            imageName().name()
                    ),
                    e
            );
        }
    }

}

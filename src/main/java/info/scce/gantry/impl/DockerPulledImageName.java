package info.scce.gantry.impl;

import info.scce.gantry.ImageName;

/**
 * DockerPulled{@link ImageName}.
 */
public final class DockerPulledImageName implements ImageName {
    private final String imageName;

    public DockerPulledImageName(String imageName) {
        this.imageName = imageName;
    }

    @Override
    public String name() {
        return imageName;
    }
}

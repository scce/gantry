package info.scce.gantry.impl;

import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientConfig;

/**
 * WindowsDockerClientFactory.
 *
 * <p>Factory for DockerClient with default configuration for Windows.</p>
 */
public final class WindowsDockerClientFactory extends AbstractDockerClientFactory {

    @Override
    protected DockerClientConfig dockerClientConfig() {
        return DefaultDockerClientConfig
                .createDefaultConfigBuilder()
                .withDockerHost("npipe:////./pipe/docker_engine")
                .build();
    }

}

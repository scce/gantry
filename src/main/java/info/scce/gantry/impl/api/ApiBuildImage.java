package info.scce.gantry.impl.api;

/**
 * ApiBuildImage.
 *
 * <p>Represents the low-level API to build images.</p>
 */
public interface ApiBuildImage extends ApiImage {
    /**
     * Uses the underlying low-level API to build images.
     *
     * @throws ApiException If the low-level API call fails
     */
    void build() throws ApiException;
}

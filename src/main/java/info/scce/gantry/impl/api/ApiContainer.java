package info.scce.gantry.impl.api;

/**
 * ApiContainer.
 *
 * <p>Represents the low-level API to manage a container.</p>
 */
public interface ApiContainer {
    /**
     * Returns a Boolean that represents if the container is running or not.
     *
     * @return Boolean True if the container is running.
     * @throws ApiException If the low-level API call fails
     */
    Boolean isRunning() throws ApiException;

    /**
     * Returns a Boolean that represents if the container is stopped or not.
     *
     * @return Boolean True if the container is stopped.
     * @throws ApiException If the low-level API call fails
     */
    Boolean isStopped() throws ApiException;

    /**
     * Uses the underlying low-level API to start the container.
     *
     * @throws ApiException If the low-level API call fails
     */
    void start() throws ApiException;

    /**
     * Uses the underlying low-level API to top the container.
     *
     * @throws ApiException If the low-level API call fails
     */
    void stop() throws ApiException;

    /**
     * Uses the underlying low-level API to remove the container.
     *
     * @throws ApiException If the low-level API call fails
     */
    void remove() throws ApiException;
}

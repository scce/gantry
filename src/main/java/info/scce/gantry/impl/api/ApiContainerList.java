package info.scce.gantry.impl.api;


import info.scce.gantry.ContainerConfig;
import info.scce.gantry.ContainerName;
import info.scce.gantry.Image;

/**
 * ApiContainerList.
 *
 * <p>Represents the low-level API to manage containers.</p>
 */
public interface ApiContainerList {
    /**
     * Uses the underlying low-level API to create an {@link ApiContainer}.
     *
     * @param image           Image used to create the container
     * @param name            Name of the container
     * @param containerConfig Configuration of the container
     * @return Low-level API to manage a container.
     * @throws ApiException If the low-level API call fails
     */
    ApiContainer create(
            Image image,
            ContainerName name,
            ContainerConfig containerConfig
    ) throws ApiException;

    /**
     * Uses the underlying low-level API to retrieve an {@link ApiContainer}.
     *
     * @param name Name of the container
     * @return Low-level API to manage a container.
     * @throws ApiException If the low-level API call fails
     */
    ApiContainer retrieve(ContainerName name) throws ApiException;

    /**
     * Uses the underlying low-level API to determine if a container with the {@link ContainerName} exists.
     *
     * @param name Name of the container
     * @return true if container exists
     * @throws ApiException If the low-level API call fails
     */
    boolean exists(ContainerName name) throws ApiException;
}

package info.scce.gantry.impl.api;

/**
 * ApiException.
 *
 * <p>Represents an exceptions of the low-level API.</p>
 */
public final class ApiException extends Exception {
    public ApiException() {
        super();
    }

    public ApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public static ApiException interrupted(Throwable cause) {
        return new ApiException(
                "The API call has been interrupted. Did the call run into timeout?",
                cause
        );
    }

}

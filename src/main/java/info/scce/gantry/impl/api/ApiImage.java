package info.scce.gantry.impl.api;

/**
 * ApiImage.
 *
 * <p>Represents the low-level API to remove an image.</p>
 */
public interface ApiImage {
    /**
     * Uses the underlying low-level API to remove the image.
     *
     * @throws ApiException If the low-level API call fails
     */
    void remove() throws ApiException;
}

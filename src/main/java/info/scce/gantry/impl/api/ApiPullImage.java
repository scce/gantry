package info.scce.gantry.impl.api;

/**
 * ApiPullImage.
 *
 * <p>Represents the low-level API to pull an image.</p>
 */
public interface ApiPullImage extends ApiImage {
    /**
     * Uses the underlying low-level API to pull the image.
     *
     * @throws ApiException If the low-level API call fails
     */
    void pull() throws ApiException;
}

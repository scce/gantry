package info.scce.gantry.impl.api;

/**
 * ApiVolume.
 *
 * <p>Represents the low-level API to manage volumes.</p>
 */
public interface ApiVolume {
    /**
     * Uses the underlying low-level API to determine if a volume exists.
     *
     * @return true if volume exists
     * @throws ApiException If the low-level API call fails
     */
    boolean exists() throws ApiException;

    /**
     * Uses the underlying low-level API to remove the volume.
     *
     * @throws ApiException If the low-level API call fails
     */
    void remove() throws ApiException;
}

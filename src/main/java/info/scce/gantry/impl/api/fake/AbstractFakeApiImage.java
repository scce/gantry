package info.scce.gantry.impl.api.fake;

import info.scce.gantry.impl.api.ApiException;
import info.scce.gantry.impl.api.ApiImage;

public abstract class AbstractFakeApiImage implements ApiImage {

    private final boolean failRemove;
    private boolean removeHasBeenCalled = false;

    public AbstractFakeApiImage(boolean failRemove) {
        this.failRemove = failRemove;
    }

    @Override
    public void remove() throws ApiException {
        removeHasBeenCalled = true;
        if (failRemove) {
            throw new ApiException();
        }
    }

    public boolean removeHasBeenCalled() {
        return removeHasBeenCalled;
    }
}

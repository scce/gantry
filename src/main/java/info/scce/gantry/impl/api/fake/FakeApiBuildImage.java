package info.scce.gantry.impl.api.fake;

import info.scce.gantry.impl.api.ApiBuildImage;
import info.scce.gantry.impl.api.ApiException;

/**
 * Fake{@link ApiBuildImage}.
 */
public final class FakeApiBuildImage extends AbstractFakeApiImage implements ApiBuildImage {
    private final boolean failBuild;
    private boolean buildHasBeenCalled = false;

    public FakeApiBuildImage(boolean failBuild, boolean failRemove) {
        super(failRemove);
        this.failBuild = failBuild;
    }

    public static FakeApiBuildImage simple() {
        return new FakeApiBuildImage(false, false);
    }

    public static FakeApiBuildImage fail() {
        return new FakeApiBuildImage(true, true);
    }

    @Override
    public void build() throws ApiException {
        buildHasBeenCalled = true;
        if (failBuild) {
            throw new ApiException();
        }
    }

    public boolean buildHasBeenCalled() {
        return buildHasBeenCalled;
    }
}

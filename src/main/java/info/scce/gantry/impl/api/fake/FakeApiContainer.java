package info.scce.gantry.impl.api.fake;

import info.scce.gantry.impl.api.ApiContainer;
import info.scce.gantry.impl.api.ApiException;

/**
 * Fake{@link ApiContainer}.
 */
public final class FakeApiContainer implements ApiContainer {
    private final boolean isRunning;
    private final boolean isStopped;
    private final boolean runningFailed;
    private boolean removeHasBeenCalled = false;
    private boolean startHasBeenCalled = false;
    private boolean stopHasBeenCalled = false;

    public FakeApiContainer(boolean isRunning, boolean isStopped, boolean stoppingFailed) {
        this.isRunning = isRunning;
        this.isStopped = isStopped;
        this.runningFailed = stoppingFailed;
    }

    public static FakeApiContainer simple() {
        return new FakeApiContainer(false, true, false);
    }

    public static FakeApiContainer stoppingFailed() {
        return new FakeApiContainer(false, true, true);
    }

    @Override
    public Boolean isRunning() {
        return isRunning;
    }

    @Override
    public Boolean isStopped() throws ApiException {
        if (runningFailed) {
            throw new ApiException();
        }
        return isStopped;
    }

    @Override
    public void start() {
        startHasBeenCalled = true;
    }

    @Override
    public void stop() {
        stopHasBeenCalled = true;
    }

    @Override
    public void remove() {
        removeHasBeenCalled = true;
    }

    public boolean removeHasBeenCalled() {
        return removeHasBeenCalled;
    }

    public boolean startHasBeenCalled() {
        return startHasBeenCalled;
    }

    public boolean stopHasBeenCalled() {
        return stopHasBeenCalled;
    }

}

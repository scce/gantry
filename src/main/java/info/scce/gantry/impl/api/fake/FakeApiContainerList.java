package info.scce.gantry.impl.api.fake;

import info.scce.gantry.ContainerConfig;
import info.scce.gantry.ContainerName;
import info.scce.gantry.Image;
import info.scce.gantry.impl.api.ApiContainer;
import info.scce.gantry.impl.api.ApiContainerList;
import info.scce.gantry.impl.api.ApiException;

/**
 * Fake{@link ApiContainerList}.
 */
public final class FakeApiContainerList implements ApiContainerList {
    private final ApiContainer apiContainer;
    private final boolean exists;
    private final boolean failed;

    public FakeApiContainerList(
            ApiContainer apiContainer,
            boolean exists,
            boolean failed
    ) {
        this.apiContainer = apiContainer;
        this.exists = exists;
        this.failed = failed;
    }

    public static FakeApiContainerList simple() {
        return new FakeApiContainerList(FakeApiContainer.simple(), true, false);
    }

    public static FakeApiContainerList failed() {
        return new FakeApiContainerList(null, true, true);
    }

    @Override
    public ApiContainer create(Image image, ContainerName name, ContainerConfig containerConfig) throws ApiException {
        if (failed) {
            throw new ApiException();
        }
        return apiContainer;
    }

    @Override
    public ApiContainer retrieve(ContainerName name) throws ApiException {
        if (failed) {
            throw new ApiException();
        }
        return apiContainer;
    }

    @Override
    public boolean exists(ContainerName name) throws ApiException {
        if (failed) {
            throw new ApiException();
        }
        return exists;
    }
}

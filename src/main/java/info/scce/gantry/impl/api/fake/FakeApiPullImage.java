package info.scce.gantry.impl.api.fake;

import info.scce.gantry.impl.api.ApiException;
import info.scce.gantry.impl.api.ApiPullImage;

/**
 * Fake{@link ApiPullImage}.
 */
public final class FakeApiPullImage extends AbstractFakeApiImage implements ApiPullImage {
    private final boolean failPulled;
    private boolean pullHasBeenCalled = false;

    public FakeApiPullImage(boolean failPulled, boolean failRemove) {
        super(failRemove);
        this.failPulled = failPulled;
    }

    public static FakeApiPullImage simple() {
        return new FakeApiPullImage(false, false);
    }

    public static FakeApiPullImage fail() {
        return new FakeApiPullImage(true, true);
    }

    @Override
    public void pull() throws ApiException {
        pullHasBeenCalled = true;
        if (failPulled) {
            throw new ApiException();
        }
    }

    public boolean pullHasBeenCalled() {
        return pullHasBeenCalled;
    }

}

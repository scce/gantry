package info.scce.gantry.impl.api.fake;

import info.scce.gantry.impl.api.ApiException;
import info.scce.gantry.impl.api.ApiVolume;

public class FakeApiVolume implements ApiVolume {
    private final boolean exists;
    private final boolean failRemove;
    private boolean existsHasBeenCalled = false;
    private boolean removeHasBeenCalled = false;

    public FakeApiVolume(boolean exists, boolean failRemove) {
        this.exists = exists;
        this.failRemove = failRemove;
    }

    public static FakeApiVolume simple() {
        return new FakeApiVolume(true, false);
    }

    public static FakeApiVolume fail() {
        return new FakeApiVolume(true, true);
    }

    public static FakeApiVolume absent() {
        return new FakeApiVolume(false, false);
    }

    @Override
    public boolean exists() {
        existsHasBeenCalled = true;
        return exists;
    }

    @Override
    public void remove() throws ApiException {
        removeHasBeenCalled = true;
        if (failRemove) {
            throw new ApiException();
        }
    }

    public boolean removeHasBeenCalled() {
        return removeHasBeenCalled;
    }

    public boolean existsHasBeenCalled() {
        return existsHasBeenCalled;
    }
}

package info.scce.gantry.impl.api.impl;

import com.github.dockerjava.api.DockerClient;

abstract class AbstractDockerClientHolder {
    protected final DockerClient dockerClient;

    protected AbstractDockerClientHolder(
            DockerClient dockerClient
    ) {
        this.dockerClient = dockerClient;
    }

}

package info.scce.gantry.impl.api.impl;

import com.github.dockerjava.api.DockerClient;
import info.scce.gantry.ImageName;
import info.scce.gantry.impl.api.ApiException;
import info.scce.gantry.impl.api.ApiImage;

public abstract class AbstractDockerImage extends AbstractDockerClientHolder implements ApiImage {

    protected final ImageName imageName;

    public AbstractDockerImage(
            DockerClient dockerClient,
            ImageName imageName
    ) {
        super(dockerClient);
        this.imageName = imageName;
    }

    @Override
    public void remove() throws ApiException {
        CaughtApiCall
                .of(() -> dockerClient
                        .removeImageCmd(imageName.name())
                        .exec())
                .call();
    }
}

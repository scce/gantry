package info.scce.gantry.impl.api.impl;

import com.github.dockerjava.api.exception.DockerException;
import info.scce.gantry.impl.api.ApiException;

/**
 * CaughtApiCall.
 *
 * <p>Encapsulate an API call in a try/catch block to catch unexpected {@link DockerException}s.</p>
 *
 * @param <V> Return value of call method
 */
public final class CaughtApiCall<V> {

    private final ApiCall<V> apiCall;

    public CaughtApiCall(ApiCall<V> apiCall) {
        this.apiCall = apiCall;
    }

    public static <V> CaughtApiCall<V> of(ApiCall<V> apiCall) {
        return new CaughtApiCall<>(apiCall);
    }

    /**
     * Calls the call method of the wrapped apiCall.
     *
     * @return Return value of the encapsulated apiCall call method
     * @throws ApiException Caught, wrapped and rethrown {@link DockerException}.
     */
    public V call() throws ApiException {
        try {
            return this.apiCall.call();
        } catch (DockerException e) {
            throw new ApiException(
                    "The API call failed unexpectedly",
                    e
            );
        }
    }

    /**
     * ApiCall.
     *
     * <p>Interface to create anonymous API calls.</p>
     *
     * @param <V> Return value of the wrapped API call
     */
    protected interface ApiCall<V> {
        V call() throws DockerException;
    }
}

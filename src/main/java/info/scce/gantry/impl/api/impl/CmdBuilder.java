package info.scce.gantry.impl.api.impl;

/**
 * CmdBuilder.
 *
 * <p>Interface for building Docker API commands.</p>
 */
public interface CmdBuilder<T> {

    /**
     * @return <T> Build Docker API command
     */
    T built();
}

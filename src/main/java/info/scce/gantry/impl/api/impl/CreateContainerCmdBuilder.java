package info.scce.gantry.impl.api.impl;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerCmd;
import com.github.dockerjava.api.model.*;
import info.scce.gantry.ContainerConfig;
import info.scce.gantry.ContainerName;
import info.scce.gantry.Image;

import java.util.List;

public class CreateContainerCmdBuilder implements CmdBuilder<CreateContainerCmd> {
    private final DockerClient dockerClient;
    private final Image image;
    private final ContainerName name;
    private final ContainerConfig config;

    private CreateContainerCmdBuilder(
            DockerClient dockerClient,
            Image image,
            ContainerName name,
            ContainerConfig config
    ) {
        this.dockerClient = dockerClient;
        this.image = image;
        this.name = name;
        this.config = config;
    }

    public static CreateContainerCmdBuilder of(
            DockerClient dockerClient,
            Image image,
            ContainerName name,
            ContainerConfig config
    ) {
        return new CreateContainerCmdBuilder(dockerClient, image, name, config);
    }

    @Override
    public CreateContainerCmd built() {
        CreateContainerCmd command = dockerClient.createContainerCmd(
                image.imageName().name()
        );
        command.withName(name.name());
        command.withHostConfig(hostConfig());

        List<String> env = config.env();
        if (!env.isEmpty()) {
            command.withEnv(env);
        }

        List<String> cmds = config.cmds();
        if (!cmds.isEmpty()) {
            command.withCmd(cmds);
        }

        List<String> entrypoints = config.entrypoints();
        if (!entrypoints.isEmpty()) {
            command.withEntrypoint(entrypoints);
        }

        List<ExposedPort> exposedPorts = config.exposedPorts();
        if (!exposedPorts.isEmpty()) {
            command.withExposedPorts(exposedPorts);
        }

        return command;
    }

    private HostConfig hostConfig() {
        HostConfig hostConfig = HostConfig.newHostConfig();

        List<Bind> binds = config.binds();
        if (!binds.isEmpty()) {
            hostConfig = hostConfig.withBinds(binds);
        }

        List<Link> links = config.links();
        if (!links.isEmpty()) {
            hostConfig = hostConfig.withLinks(links);
        }

        List<PortBinding> portBindings = config.portBindings();
        if (!portBindings.isEmpty()) {
            hostConfig = hostConfig.withPortBindings(portBindings);
        }

        return hostConfig;
    }
}

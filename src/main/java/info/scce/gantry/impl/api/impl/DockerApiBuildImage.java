package info.scce.gantry.impl.api.impl;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.exception.DockerClientException;
import info.scce.gantry.Dockerfile;
import info.scce.gantry.ImageName;
import info.scce.gantry.impl.api.ApiBuildImage;
import info.scce.gantry.impl.api.ApiException;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * Docker{@link ApiBuildImage}.
 */
public final class DockerApiBuildImage extends AbstractDockerImage implements ApiBuildImage {
    private final Dockerfile dockerfile;

    public DockerApiBuildImage(
            DockerClient dockerClient,
            ImageName imageName,
            Dockerfile dockerfile
    ) {
        super(dockerClient, imageName);
        this.dockerfile = dockerfile;
    }

    @Override
    public void build() throws ApiException {
        try {
            dockerClient
                    .buildImageCmd()
                    .withBaseDirectory(dockerfile.baseDirectory())
                    .withDockerfile(dockerfile.dockerfile())
                    .withTags(Collections.singleton(imageName.name()))
                    .start()
                    .awaitImageId(30, TimeUnit.MINUTES);
        } catch (IllegalArgumentException | DockerClientException e) {
            throw new ApiException(
                    String.format(
                            "We failed executing the API call for Dockerfile \"%s\" in base directory \"%s\"",
                            dockerfile.dockerfile(),
                            dockerfile.baseDirectory()
                    ),
                    e
            );
        }
    }

}

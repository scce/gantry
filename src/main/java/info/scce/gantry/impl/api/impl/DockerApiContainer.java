package info.scce.gantry.impl.api.impl;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.InspectContainerResponse;
import info.scce.gantry.impl.api.ApiContainer;
import info.scce.gantry.impl.api.ApiException;

/**
 * Docker{@link ApiContainer}.
 */
public final class DockerApiContainer extends AbstractDockerClientHolder implements ApiContainer {
    private final String id;

    public DockerApiContainer(
            DockerClient dockerClient,
            String id
    ) {
        super(dockerClient);
        this.id = id;
    }

    @Override
    public void start() throws ApiException {
        CaughtApiCall
                .of(() -> dockerClient
                        .startContainerCmd(id)
                        .exec())
                .call();
    }

    @Override
    public void stop() throws ApiException {
        CaughtApiCall
                .of(() -> dockerClient
                        .stopContainerCmd(id)
                        .exec())
                .call();
    }

    @Override
    public void remove() throws ApiException {
        CaughtApiCall
                .of(() -> dockerClient
                        .removeContainerCmd(id)
                        .exec())
                .call();
    }

    @Override
    public Boolean isRunning() throws ApiException {
        return state()
                .getRunning();
    }

    @Override
    public Boolean isStopped() throws ApiException {
        return !isRunning();
    }

    private InspectContainerResponse.ContainerState state() throws ApiException {
        return CaughtApiCall
                .of(() -> dockerClient
                        .inspectContainerCmd(id)
                        .exec()
                        .getState())
                .call();
    }


}

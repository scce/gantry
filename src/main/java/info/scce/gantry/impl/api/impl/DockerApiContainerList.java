package info.scce.gantry.impl.api.impl;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerCmd;
import com.github.dockerjava.api.exception.NotFoundException;
import info.scce.gantry.ContainerConfig;
import info.scce.gantry.ContainerName;
import info.scce.gantry.Image;
import info.scce.gantry.impl.api.ApiContainer;
import info.scce.gantry.impl.api.ApiContainerList;
import info.scce.gantry.impl.api.ApiException;

/**
 * Docker{@link ApiContainerList}.
 */
public final class DockerApiContainerList extends AbstractDockerClientHolder implements ApiContainerList {
    public DockerApiContainerList(
            DockerClient dockerClient
    ) {
        super(dockerClient);
    }

    @Override
    public ApiContainer create(
            Image image,
            ContainerName name,
            ContainerConfig containerConfig
    ) throws ApiException {
        return new DockerApiContainer(
                dockerClient,
                newId(
                        image,
                        name,
                        containerConfig
                )
        );
    }

    @Override
    public ApiContainer retrieve(ContainerName name) throws ApiException {
        return new DockerApiContainer(
                dockerClient,
                existingId(name)
        );
    }

    @Override
    public boolean exists(ContainerName name) throws ApiException {
        return CaughtApiCall.of(() -> {
            try {
                dockerClient
                        .inspectContainerCmd(name.name())
                        .exec();
                return true;
            } catch (NotFoundException ignored) {
                return false;
            }
        }).call();
    }

    private String newId(
            Image image,
            ContainerName name,
            ContainerConfig containerConfig
    ) throws ApiException {
        return CaughtApiCall.of(() -> {
                    CreateContainerCmdBuilder builder = CreateContainerCmdBuilder.of(
                            dockerClient,
                            image,
                            name,
                            containerConfig
                    );
                    try (CreateContainerCmd command = builder.built()) {
                        return command.exec().getId();
                    }
                }
        ).call();
    }

    private String existingId(ContainerName name) throws ApiException {
        return CaughtApiCall.of(() -> dockerClient
                .inspectContainerCmd(name.name())
                .exec()
                .getId()).call();
    }

}

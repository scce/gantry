package info.scce.gantry.impl.api.impl;

import com.github.dockerjava.api.DockerClient;
import info.scce.gantry.ImageName;
import info.scce.gantry.impl.api.ApiException;
import info.scce.gantry.impl.api.ApiPullImage;

import java.util.concurrent.TimeUnit;

/**
 * Docker{@link ApiPullImage}.
 */
public final class DockerApiPullImage extends AbstractDockerImage implements ApiPullImage {

    public DockerApiPullImage(
            DockerClient dockerClient,
            ImageName image
    ) {
        super(dockerClient, image);
    }

    @Override
    public void pull() throws ApiException {
        try {
            dockerClient
                    .pullImageCmd(imageName.name())
                    .start()
                    .awaitCompletion(30, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            throw ApiException.interrupted(e);
        }
    }

}

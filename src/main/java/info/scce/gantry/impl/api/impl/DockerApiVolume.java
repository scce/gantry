package info.scce.gantry.impl.api.impl;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.exception.NotFoundException;
import info.scce.gantry.impl.api.ApiException;
import info.scce.gantry.impl.api.ApiVolume;

/**
 * Docker{@link ApiVolume}.
 */
public final class DockerApiVolume extends AbstractDockerClientHolder implements ApiVolume {
    private final String id;

    public DockerApiVolume(
            DockerClient dockerClient,
            String id
    ) {
        super(dockerClient);
        this.id = id;
    }

    @Override
    public boolean exists() throws ApiException {
        return CaughtApiCall.of(() -> {
            try {
                dockerClient
                        .inspectVolumeCmd(id)
                        .exec();
                return true;
            } catch (NotFoundException ignored) {
                return false;
            }
        }).call();
    }

    @Override
    public void remove() throws ApiException {
        CaughtApiCall
                .of(() -> dockerClient
                        .removeVolumeCmd(id)
                        .exec())
                .call();
    }


}

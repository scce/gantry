package info.scce.gantry.fake;

import info.scce.gantry.ContainerConfig;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FakeContainerConfigTest {

    @Test
    void simple() {
        //given
        ContainerConfig fakeContainerConfig = FakeContainerConfig.simple();
        //when/then
        assertTrue(fakeContainerConfig.entrypoints().isEmpty());
        assertTrue(fakeContainerConfig.cmds().isEmpty());
        assertTrue(fakeContainerConfig.binds().isEmpty());
        assertTrue(fakeContainerConfig.env().isEmpty());
        assertTrue(fakeContainerConfig.links().isEmpty());
        assertTrue(fakeContainerConfig.portBindings().isEmpty());
    }
}
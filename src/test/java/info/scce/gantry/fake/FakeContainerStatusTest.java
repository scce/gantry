package info.scce.gantry.fake;

import info.scce.gantry.ContainerStatusEnum;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FakeContainerStatusTest {

    @Test
    void simple() {
        //given
        FakeContainerStatus fakeContainerStatus = FakeContainerStatus.simple();
        //then
        assertEquals(
                ContainerStatusEnum.RUNNING,
                //when
                fakeContainerStatus.status()
        );
    }
}
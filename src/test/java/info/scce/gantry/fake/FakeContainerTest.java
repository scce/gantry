package info.scce.gantry.fake;

import info.scce.gantry.Container;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FakeContainerTest {

    @Test
    void containerName() {
        //given
        FakeContainerName containerName = FakeContainerName.simple();
        Container container = new FakeContainer(
                FakeContainerStatus.simple(),
                containerName
        );
        //then
        assertEquals(
                containerName,
                //when
                container.containerName()
        );
    }

    @Test
    void containerStatus() throws Container.ContainerException {
        //given
        FakeContainerStatus containerStatus = FakeContainerStatus.simple();
        Container container = new FakeContainer(
                containerStatus,
                FakeContainerName.simple()
        );
        //then
        assertEquals(
                containerStatus,
                //when
                container.containerStatus()
        );
    }

    @Test
    void start() {
        //given
        FakeContainer container = new FakeContainer(
                FakeContainerStatus.simple(),
                FakeContainerName.simple()
        );
        //when
        container.start();
        //then
        assertTrue(container.startHasBeenCalled());
    }

    @Test
    void stop() {
        //given
        FakeContainer container = new FakeContainer(
                FakeContainerStatus.simple(),
                FakeContainerName.simple()
        );
        //when
        container.stop();
        //then
        assertTrue(container.stopHasBeenCalled());
    }

    @Test
    void remove() {
        //given
        FakeContainer container = new FakeContainer(
                FakeContainerStatus.simple(),
                FakeContainerName.simple()
        );
        //when
        container.remove();
        //then
        assertTrue(container.removeHasBeenCalled());
    }

    @Test
    void prepareImage() {
        //given
        FakeContainer container = new FakeContainer(
                FakeContainerStatus.simple(),
                FakeContainerName.simple()
        );
        //when
        container.prepareImage();
        //then
        assertTrue(container.prepareImageHasBeenCalled());
    }

    @Test
    void removeProvidedVolumes() {
        //given
        FakeContainer container = new FakeContainer(
                FakeContainerStatus.simple(),
                FakeContainerName.simple()
        );
        //when
        container.removeProvidedVolumes();
        //then
        assertTrue(container.removeProvidedVolumesHasBeenCalled());
    }
}
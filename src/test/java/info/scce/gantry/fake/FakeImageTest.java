package info.scce.gantry.fake;

import info.scce.gantry.Image;
import info.scce.gantry.ImageName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FakeImageTest {

    @Test
    void simple() throws Image.ImageException {
        //given
        FakeImage fakeImage = FakeImage.simple();
        //when
        fakeImage.prepare();
        fakeImage.remove();
        ImageName imageName = fakeImage.imageName();
        //then
        assertEquals("simple", imageName.name());
        assertTrue(fakeImage.prepareHasBeenCalled());
        assertTrue(fakeImage.removeHasBeenCalled());
    }

    @Test
    void failed() {
        //given
        FakeImage fakeImage = FakeImage.failed();
        //when
        ImageName imageName = fakeImage.imageName();
        try {
            fakeImage.prepare();
            fail("Expected ImageException to be thrown");
        } catch (Image.ImageException ignored) {
        }
        try {
            fakeImage.remove();
            fail("Expected ImageException to be thrown");
        } catch (Image.ImageException ignored) {
        }
        //then
        assertEquals("simple", imageName.name());
        assertTrue(fakeImage.prepareHasBeenCalled());
        assertTrue(fakeImage.removeHasBeenCalled());
    }
}
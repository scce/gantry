package info.scce.gantry.fake;

import info.scce.gantry.Link;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FakeLinkTest {

    @Test
    void simple() {
        //given
        Link link = FakeLink.simple();
        //when
        String alias = link.alias();
        String name = link.name();
        //then
        assertEquals("alias", alias);
        assertEquals("name", name);
    }
}
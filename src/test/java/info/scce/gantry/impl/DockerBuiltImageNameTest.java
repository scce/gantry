package info.scce.gantry.impl;

import info.scce.gantry.ImageName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DockerBuiltImageNameTest {
    @Test
    void name() {
        //given
        ImageName imageName = new DockerBuiltImageName("project", "nginx");
        //then
        assertEquals(
                "project_nginx",
                //when
                imageName.name()
        );
    }
}
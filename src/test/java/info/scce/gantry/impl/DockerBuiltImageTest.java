package info.scce.gantry.impl;

import info.scce.gantry.Image;
import info.scce.gantry.fake.FakeImageName;
import info.scce.gantry.impl.api.fake.FakeApiBuildImage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

class DockerBuiltImageTest {
    @Test
    void prepare() throws Image.ImageException {
        //given
        FakeApiBuildImage fakeApiBuildImage = FakeApiBuildImage.simple();
        Image image = new DockerBuiltImage(fakeApiBuildImage, FakeImageName.simple());
        //when
        image.prepare();
        //then
        assertTrue(fakeApiBuildImage.buildHasBeenCalled());
    }

    @Test
    void prepareFailed() {
        //given
        FakeApiBuildImage fakeApiBuildImage = FakeApiBuildImage.fail();
        Image image = new DockerBuiltImage(fakeApiBuildImage, FakeImageName.simple());
        try {
            //when
            image.prepare();
            fail(String.format("Expect %s to be thrown", Image.ImageException.class));
        } catch (Image.ImageException ignored) {
            //then
            assertTrue(fakeApiBuildImage.buildHasBeenCalled());
        }
    }

    @Test
    void remove() throws Image.ImageException {
        //given
        FakeApiBuildImage fakeApiBuildImage = FakeApiBuildImage.simple();
        Image image = new DockerBuiltImage(fakeApiBuildImage, FakeImageName.simple());
        //when
        image.remove();
        //then
        assertTrue(fakeApiBuildImage.removeHasBeenCalled());
    }

    @Test
    void removeFailed() {
        //given
        FakeApiBuildImage fakeApiBuildImage = FakeApiBuildImage.fail();
        Image image = new DockerBuiltImage(fakeApiBuildImage, FakeImageName.simple());
        try {
            //when
            image.remove();
            fail(String.format("Expect %s to be thrown", Image.ImageException.class));
        } catch (Image.ImageException ignored) {
            //then
            assertTrue(fakeApiBuildImage.removeHasBeenCalled());
        }
    }
}
package info.scce.gantry.impl;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DockerCommandTest {
    @Test
    void serialized() {
        //given
        DockerCommand command = new DockerCommand("-c foo=bar");
        //when
        String serialized = command.serialized();
        //then
        assertEquals("-c foo=bar", serialized);
    }

}
package info.scce.gantry.impl;

import com.github.dockerjava.api.model.Bind;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.Link;
import com.github.dockerjava.api.model.PortBinding;
import info.scce.gantry.ContainerConfig;
import info.scce.gantry.ProvidedVolume;
import info.scce.gantry.fake.*;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DockerContainerConfigTest {
    @Test
    void entrypoint() {
        //given
        ContainerConfig containerConfig = new DockerContainerConfig(
                Collections.singletonList(FakeEntrypoint.simple()),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList()
        );
        //when
        List<String> entrypointList = containerConfig.entrypoints();
        List<String> cmdList = containerConfig.cmds();
        List<String> envList = containerConfig.env();
        List<Link> linkList = containerConfig.links();
        List<PortBinding> portBindingList = containerConfig.portBindings();
        List<ExposedPort> exposedPortList = containerConfig.exposedPorts();
        List<Bind> bindList = containerConfig.binds();
        //then
        assertEquals(1, entrypointList.size());
        assertEquals(0, cmdList.size());
        assertEquals(0, envList.size());
        assertEquals(0, linkList.size());
        assertEquals(0, portBindingList.size());
        assertEquals(0, exposedPortList.size());
        assertEquals(0, bindList.size());
    }

    @Test
    void cmd() {
        //given
        ContainerConfig containerConfig = new DockerContainerConfig(
                Collections.emptyList(),
                Collections.singletonList(FakeCommand.simple()),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList()
        );
        //when
        List<String> entrypointList = containerConfig.entrypoints();
        List<String> cmdList = containerConfig.cmds();
        List<String> envList = containerConfig.env();
        List<Link> linkList = containerConfig.links();
        List<PortBinding> portBindingList = containerConfig.portBindings();
        List<ExposedPort> exposedPortList = containerConfig.exposedPorts();
        List<Bind> bindList = containerConfig.binds();
        //then
        assertEquals(0, entrypointList.size());
        assertEquals(1, cmdList.size());
        assertEquals(0, envList.size());
        assertEquals(0, linkList.size());
        assertEquals(0, portBindingList.size());
        assertEquals(0, exposedPortList.size());
        assertEquals(0, bindList.size());
    }

    @Test
    void env() {
        //given
        ContainerConfig containerConfig = new DockerContainerConfig(
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.singletonList(FakeEnvironmentVariable.simple()),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList()
        );
        //when
        List<String> entrypointList = containerConfig.entrypoints();
        List<String> cmdList = containerConfig.cmds();
        List<String> envList = containerConfig.env();
        List<Link> linkList = containerConfig.links();
        List<PortBinding> portBindingList = containerConfig.portBindings();
        List<ExposedPort> exposedPortList = containerConfig.exposedPorts();
        List<Bind> bindList = containerConfig.binds();
        //then
        assertEquals(0, entrypointList.size());
        assertEquals(0, cmdList.size());
        assertEquals(1, envList.size());
        assertEquals(0, linkList.size());
        assertEquals(0, portBindingList.size());
        assertEquals(0, exposedPortList.size());
        assertEquals(0, bindList.size());
    }

    @Test
    void linkList() {
        //given
        ContainerConfig containerConfig = new DockerContainerConfig(
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.singletonList(FakeLink.simple()),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList()
        );
        //when
        List<String> entrypointList = containerConfig.entrypoints();
        List<String> cmdList = containerConfig.cmds();
        List<String> envList = containerConfig.env();
        List<Link> linkList = containerConfig.links();
        List<PortBinding> portBindingList = containerConfig.portBindings();
        List<ExposedPort> exposedPortList = containerConfig.exposedPorts();
        List<Bind> bindList = containerConfig.binds();
        //then
        assertEquals(0, entrypointList.size());
        assertEquals(0, cmdList.size());
        assertEquals(0, envList.size());
        assertEquals(1, linkList.size());
        assertEquals(0, portBindingList.size());
        assertEquals(0, exposedPortList.size());
        assertEquals(0, bindList.size());
    }

    @Test
    void portSpec() {
        //given
        ContainerConfig containerConfig = new DockerContainerConfig(
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.singletonList(FakePortSpec.simple()),
                Collections.emptyList(),
                Collections.emptyList()
        );
        //when
        List<String> entrypointList = containerConfig.entrypoints();
        List<String> cmdList = containerConfig.cmds();
        List<String> envList = containerConfig.env();
        List<Link> linkList = containerConfig.links();
        List<PortBinding> portBindingList = containerConfig.portBindings();
        List<ExposedPort> exposedPortList = containerConfig.exposedPorts();
        List<Bind> bindList = containerConfig.binds();
        //then
        assertEquals(0, entrypointList.size());
        assertEquals(0, cmdList.size());
        assertEquals(0, envList.size());
        assertEquals(0, linkList.size());
        assertEquals(1, portBindingList.size());
        assertEquals(1, exposedPortList.size());
        assertEquals(0, bindList.size());
    }

    @Test
    void binds() {
        //given
        ContainerConfig containerConfig = new DockerContainerConfig(
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.singletonList(FakeHostDirectory.simple()),
                Collections.emptyList()
        );
        //when
        List<String> entrypointList = containerConfig.entrypoints();
        List<String> cmdList = containerConfig.cmds();
        List<Bind> bindList = containerConfig.binds();
        List<String> envList = containerConfig.env();
        List<PortBinding> portBindingList = containerConfig.portBindings();
        List<ExposedPort> exposedPortList = containerConfig.exposedPorts();
        List<Link> linkList = containerConfig.links();
        //then
        assertEquals(0, entrypointList.size());
        assertEquals(0, cmdList.size());
        assertEquals(0, envList.size());
        assertEquals(0, linkList.size());
        assertEquals(0, portBindingList.size());
        assertEquals(0, exposedPortList.size());
        assertEquals(1, bindList.size());
    }

    @Test
    void removeProvidedVolumes() throws ProvidedVolume.ProvidedVolumeException {
        //given
        FakeProvidedVolume volume = FakeProvidedVolume.simple();
        ContainerConfig containerConfig = new DockerContainerConfig(
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.singletonList(volume)
        );
        //when
        containerConfig.removeProvidedVolumes();
        //then
        assertTrue(volume.removeHasBeenCalled());
    }

}
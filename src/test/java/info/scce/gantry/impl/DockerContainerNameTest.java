package info.scce.gantry.impl;

import info.scce.gantry.ContainerName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DockerContainerNameTest {
    @Test
    void name() {
        //given
        ContainerName containerName = new DockerContainerName("project", "nginx");
        //then
        assertEquals(
                "project_nginx",
                //when
                containerName.name()
        );
    }
}
package info.scce.gantry.impl;

import info.scce.gantry.ContainerStatus;
import info.scce.gantry.ContainerStatusEnum;
import info.scce.gantry.fake.FakeContainerName;
import info.scce.gantry.impl.api.fake.FakeApiContainer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class DockerContainerStatusTest {
    @Test
    void statusStopped() throws ContainerStatus.ContainerStatusException {
        //given
        ContainerStatus containerStatus = new DockerContainerStatus(FakeContainerName.simple(), new FakeApiContainer(false, true, false));
        //then
        Assertions.assertEquals(
                ContainerStatusEnum.STOPPED,
                //when
                containerStatus.status()
        );
    }

    @Test
    void statusRunning() throws ContainerStatus.ContainerStatusException {
        //given
        ContainerStatus containerStatus = new DockerContainerStatus(FakeContainerName.simple(), new FakeApiContainer(true, false, false));
        //then
        assertEquals(
                ContainerStatusEnum.RUNNING,
                //when
                containerStatus.status()
        );
    }

    @Test
    void statusUnknown() throws ContainerStatus.ContainerStatusException {
        //given
        ContainerStatus containerStatus = new DockerContainerStatus(FakeContainerName.simple(), new FakeApiContainer(false, false, false));
        //then
        assertEquals(
                ContainerStatusEnum.UNKNOWN,
                //when
                containerStatus.status()
        );
    }

    @Test
    void statusFailed() {
        //given
        ContainerStatus containerStatus = new DockerContainerStatus(FakeContainerName.simple(), FakeApiContainer.stoppingFailed());
        try {
            //when
            containerStatus.status();
            fail(String.format("Expect %s to be thrown", ContainerStatus.ContainerStatusException.class));
        } catch (ContainerStatus.ContainerStatusException e) {
            //then
            assertEquals("We failed determining the status of container \"name\"", e.getMessage());
        }
    }
}
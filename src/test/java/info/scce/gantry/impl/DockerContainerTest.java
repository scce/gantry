package info.scce.gantry.impl;

import info.scce.gantry.Container;
import info.scce.gantry.ContainerStatus;
import info.scce.gantry.ContainerStatusEnum;
import info.scce.gantry.fake.FakeContainerConfig;
import info.scce.gantry.fake.FakeContainerName;
import info.scce.gantry.fake.FakeImage;
import info.scce.gantry.impl.api.fake.FakeApiContainer;
import info.scce.gantry.impl.api.fake.FakeApiContainerList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DockerContainerTest {
    @Test
    void startNewContainer() throws Container.ContainerException {
        //given
        FakeApiContainer fakeApiContainer = FakeApiContainer.simple();
        Container container = new DockerContainer(new FakeApiContainerList(fakeApiContainer, false, false), FakeImage.simple(), FakeContainerName.simple(), FakeContainerConfig.simple());
        //then
        container.start();
        assertFalse(fakeApiContainer.stopHasBeenCalled());
        assertTrue(fakeApiContainer.startHasBeenCalled());
        assertFalse(fakeApiContainer.removeHasBeenCalled());
    }

    @Test
    void startExistingContainer() throws Container.ContainerException {
        //given
        FakeApiContainer fakeApiContainer = FakeApiContainer.simple();
        Container container = new DockerContainer(new FakeApiContainerList(fakeApiContainer, true, false), FakeImage.simple(), FakeContainerName.simple(), FakeContainerConfig.simple());
        //then
        container.start();
        assertFalse(fakeApiContainer.stopHasBeenCalled());
        assertTrue(fakeApiContainer.startHasBeenCalled());
        assertFalse(fakeApiContainer.removeHasBeenCalled());
    }

    @Test
    void startFailed() {
        //given
        Container container = new DockerContainer(FakeApiContainerList.failed(), FakeImage.simple(), FakeContainerName.simple(), FakeContainerConfig.simple());
        try {
            //when
            container.start();
            fail(String.format("Expect %s to be thrown", Container.ContainerException.class));
        } catch (Container.ContainerException e) {
            //then
            assertEquals("We failed starting the container \"name\"", e.getMessage());
        }
    }

    @Test
    void status() throws Container.ContainerException, ContainerStatus.ContainerStatusException {
        //given
        Container container = new DockerContainer(FakeApiContainerList.simple(), FakeImage.simple(), FakeContainerName.simple(), FakeContainerConfig.simple());
        //when
        ContainerStatus status = container.containerStatus();
        //then
        assertTrue(status instanceof DockerContainerStatus);
        Assertions.assertEquals(ContainerStatusEnum.STOPPED, status.status());
    }


    @Test
    void statusFailed() {
        //given
        Container container = new DockerContainer(FakeApiContainerList.failed(), FakeImage.simple(), FakeContainerName.simple(), FakeContainerConfig.simple());
        try {
            //when
            container.containerStatus();
            fail(String.format("Expect %s to be thrown", Container.ContainerException.class));
        } catch (Container.ContainerException e) {
            //then
            assertEquals("We failed creating the status of container \"name\"", e.getMessage());
        }
    }

    @Test
    void statusNotExisting() throws Container.ContainerException, ContainerStatus.ContainerStatusException {
        //given
        FakeApiContainerList simple = new FakeApiContainerList(FakeApiContainer.simple(), false, false);
        Container container = new DockerContainer(simple, FakeImage.simple(), FakeContainerName.simple(), FakeContainerConfig.simple());
        //when
        ContainerStatus status = container.containerStatus();
        //then
        assertTrue(status instanceof DockerNotExistingContainerStatus);
        assertEquals(ContainerStatusEnum.NOT_EXISTING, status.status());
    }

    @Test
    void stop() throws Container.ContainerException {
        //given
        FakeApiContainer fakeApiContainer = new FakeApiContainer(true, false, false);
        FakeApiContainerList fakeApiContainerList = new FakeApiContainerList(fakeApiContainer, true, false);
        Container container = new DockerContainer(fakeApiContainerList, FakeImage.simple(), FakeContainerName.simple(), FakeContainerConfig.simple());
        //when
        container.stop();
        //then
        assertTrue(fakeApiContainer.stopHasBeenCalled());
    }

    @Test
    void stopFailed() {
        //given
        Container container = new DockerContainer(FakeApiContainerList.failed(), FakeImage.simple(), FakeContainerName.simple(), FakeContainerConfig.simple());
        try {
            //when
            container.stop();
            fail(String.format("Expect %s to be thrown", Container.ContainerException.class));
        } catch (Container.ContainerException e) {
            //then
            assertEquals("We failed stopping the container \"name\"", e.getMessage());
        }
    }

    @Test
    void remove() throws Container.ContainerException {
        //given
        FakeApiContainer fakeApiContainer = FakeApiContainer.simple();
        FakeApiContainerList fakeApiContainerList = new FakeApiContainerList(fakeApiContainer, true, false);
        Container container = new DockerContainer(fakeApiContainerList, FakeImage.simple(), FakeContainerName.simple(), FakeContainerConfig.simple());
        //when
        container.remove();
        //then
        assertTrue(fakeApiContainer.removeHasBeenCalled());
    }

    @Test
    void removeFailed() {
        //given
        Container container = new DockerContainer(FakeApiContainerList.failed(), FakeImage.simple(), FakeContainerName.simple(), FakeContainerConfig.simple());
        try {
            //when
            container.remove();
            fail(String.format("Expect %s to be thrown", Container.ContainerException.class));
        } catch (Container.ContainerException e) {
            //then
            assertEquals("We failed removing the container \"name\"", e.getMessage());
        }
    }

    @Test
    void prepareImage() throws Container.ContainerException {
        //given
        FakeApiContainer fakeApiContainer = FakeApiContainer.simple();
        FakeApiContainerList fakeApiContainerList = new FakeApiContainerList(fakeApiContainer, true, false);
        FakeImage fakeImage = FakeImage.simple();
        Container container = new DockerContainer(fakeApiContainerList, fakeImage, FakeContainerName.simple(), FakeContainerConfig.simple());
        //when
        container.prepareImage();
        //then
        assertTrue(fakeImage.prepareHasBeenCalled());
    }

    @Test
    void prepareFailed() {
        //given
        Container container = new DockerContainer(FakeApiContainerList.failed(), FakeImage.failed(), FakeContainerName.simple(), FakeContainerConfig.simple());
        try {
            //when
            container.prepareImage();
            fail(String.format("Expect %s to be thrown", Container.ContainerException.class));
        } catch (Container.ContainerException e) {
            //then
            assertEquals("We failed preparing the image of container \"name\"", e.getMessage());
        }
    }

    @Test
    void containerName() {
        //given
        FakeApiContainerList fakeApiContainerList = new FakeApiContainerList(FakeApiContainer.simple(), true, false);
        Container container = new DockerContainer(fakeApiContainerList, FakeImage.simple(), FakeContainerName.simple(), FakeContainerConfig.simple());
        //when
        assertEquals(
                "name",
                //then
                container.containerName().name()
        );
    }

    @Test
    void removeProvidedVolume() throws Container.ContainerException {
        //given
        Container container = new DockerContainer(
                FakeApiContainerList.simple(),
                FakeImage.simple(),
                FakeContainerName.simple(),
                FakeContainerConfig.simple()
        );
        //when
        container.removeProvidedVolumes();
    }

    @Test
    void removeProvidedVolumeFailed() {
        //given
        FakeContainerConfig containerConfig = FakeContainerConfig.fail();
        Container container = new DockerContainer(
                FakeApiContainerList.simple(),
                FakeImage.simple(),
                FakeContainerName.simple(),
                containerConfig
        );
        //when
        try {
            container.removeProvidedVolumes();
            fail(String.format("Expect %s to be thrown", Container.ContainerException.class));
        } catch (Container.ContainerException ignored) {
            //then
            assertTrue(containerConfig.removeProvidedVolumesHasBeenCalled());
        }
    }
}
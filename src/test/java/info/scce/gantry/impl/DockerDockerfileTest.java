package info.scce.gantry.impl;

import info.scce.gantry.Dockerfile;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DockerDockerfileTest {
    @Test
    void dockerfile() {
        //given
        Dockerfile dockerfile = new DockerDockerfile(new File("/tmp"), "service", "development.Dockerfile");
        //when
        File actual = dockerfile.dockerfile();
        //then
        assertEquals(new File("/tmp/service/development.Dockerfile"), actual);
    }

    @Test
    void baseDirectory() {
        //given
        Dockerfile dockerfile = new DockerDockerfile(new File("/tmp"), "service", "Dockerfile");
        //when
        File actual = dockerfile.baseDirectory();
        //then
        assertEquals(new File("/tmp/service"), actual);
    }

}
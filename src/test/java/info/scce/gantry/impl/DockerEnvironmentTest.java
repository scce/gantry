package info.scce.gantry.impl;

import info.scce.gantry.Container;
import info.scce.gantry.Environment;
import info.scce.gantry.fake.FakeContainerConfig;
import info.scce.gantry.fake.FakeImage;
import info.scce.gantry.impl.api.fake.FakeApiContainer;
import info.scce.gantry.impl.api.fake.FakeApiContainerList;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

public class DockerEnvironmentTest {

    @Test()
    void CheckPreconditions_DockerHostConnectionFailure_ThrowException() {
        final Environment environment = new DockerEnvironment(new WindowsDockerClientFactory().build(), "test", Collections.emptyList());
        assertThrows(Environment.EnvironmentException.class, environment::checkPreconditions);
    }

    @Test()
    void GetContainer_ContainerDoesNotExist_ReturnNull() {
        final Environment environment = new DockerEnvironment(new DefaultDockerClientFactory().build(), "test", Collections.emptyList());
        final Container container = environment.container("nonExistingContainerName");
        assertNull(container);
    }

    @Test()
    void GetContainer_ContainerExists_ReturnContainer() {
        final String containerName = "foo";
        final Container fakeContainer = createFakeContainer(containerName);
        final Environment environment = new DockerEnvironment(new DefaultDockerClientFactory().build(), "test", Collections.singletonList(fakeContainer));
        final Container container = environment.container(containerName);

        assertNotNull(container);
        assertEquals(fakeContainer, container);
    }

    @Test()
    public void AddContainer_ContainerDoesNotExist_EnvironmentIsImmutable() throws Environment.EnvironmentException {
        final String containerName = "foo";
        final Container container = createFakeContainer(containerName);
        final Environment env1 = new DockerEnvironment(new DefaultDockerClientFactory().build(), "test", Collections.emptyList());
        final Environment env2 = env1.withContainer(container);

        // check immutability of the environment
        assertNull(env1.container(containerName));

        // check if container exists in new environment
        assertNotNull(env2.container(containerName));
        assertEquals(container, env2.container(containerName));
    }

    @Test()
    public void AddContainer_ContainerExists_ThrowException() throws Environment.EnvironmentException {
        final Container container = createFakeContainer("foo");
        final Environment env1 = new DockerEnvironment(new DefaultDockerClientFactory().build(), "test", Collections.emptyList());
        final Environment env2 = env1.withContainer(container);

        // add container a second time
        assertThrows(Environment.EnvironmentException.class, () -> env2.withContainer(container));
    }

    @Test()
    public void RemoveContainer_ContainerExists_EnvironmentIsImmutable() throws Environment.EnvironmentException {
        final String containerName = "foo";
        final Container container = createFakeContainer(containerName);
        final Environment env1 = new DockerEnvironment(new DefaultDockerClientFactory().build(), "test", Collections.singletonList(container));
        final Environment env2 = env1.withoutContainer(containerName);

        // check if container still exists in old environment
        assertNotNull(env1.container(containerName));
        assertEquals(container, env1.container(containerName));

        // check if container has been removed
        assertNull(env2.container(containerName));
    }

    @Test()
    public void RemoveContainer_ContainerDoesNotExist_ThrowException() {
        final String containerName = "foo";
        final Environment env1 = new DockerEnvironment(new DefaultDockerClientFactory().build(), "test", Collections.emptyList());

        // remove non existing container
        assertThrows(Environment.EnvironmentException.class, () -> env1.withoutContainer(containerName));
    }

    private DockerContainer createFakeContainer(String containerName) {
        final FakeApiContainer fakeApiContainer = FakeApiContainer.simple();
        return new DockerContainer(
                new FakeApiContainerList(fakeApiContainer, true, false),
                new FakeImage(() -> "test", false, false),
                new DockerContainerName("test", containerName),
                FakeContainerConfig.simple()
        );
    }
}

package info.scce.gantry.impl;

import info.scce.gantry.EnvironmentVariable;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DockerEnvironmentVariableTest {
    @Test
    void serialized() {
        //given
        EnvironmentVariable environmentVariable = new DockerEnvironmentVariable("KEY", "VALUE");
        //when
        String serialized = environmentVariable.serialized();
        //then
        assertEquals("KEY=VALUE", serialized);
    }

}
package info.scce.gantry.impl;

import info.scce.gantry.HostDirectory;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DockerHostDirectoryTest {
    @Test
    void serialized() {
        //given
        HostDirectory hostDirectory = new DockerHostDirectory(new File("/tmp"), "nginx.conf", "/etc/nginx/nginx.conf");
        //when
        String serialized = hostDirectory.serialized();
        //then
        assertEquals("/tmp/nginx.conf:/etc/nginx/nginx.conf", serialized);
    }

}
package info.scce.gantry.impl;

import info.scce.gantry.Link;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DockerLinkTest {
    @Test
    void alias() {
        //given
        Link link = new DockerLink("project", "name", "alias");
        //when
        String alias = link.alias();
        //then
        assertEquals("alias", alias);
    }

    @Test
    void name() {
        //given
        Link link = new DockerLink("project", "name", "alias");
        //when
        String name = link.name();
        //then
        assertEquals("project_name", name);
    }
}
package info.scce.gantry.impl;

import info.scce.gantry.ContainerStatus;
import info.scce.gantry.ContainerStatusEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class DockerNotExistingContainerStatusTest {
    @Test
    void status() throws ContainerStatus.ContainerStatusException {
        //given
        ContainerStatus containerStatus = new DockerNotExistingContainerStatus();
        //then
        Assertions.assertEquals(
                ContainerStatusEnum.NOT_EXISTING,
                //when
                containerStatus.status()
        );
    }

}
package info.scce.gantry.impl;

import info.scce.gantry.PortSpec;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DockerPortSpecTest {
    @Test
    void serialized() {
        //given
        PortSpec portSpec = new DockerPortSpec("8080", "80");
        //when
        String serialized = portSpec.serialized();
        //then
        assertEquals("8080:80", serialized);
    }

    @Test
    void exposedPorts() {
        //given
        PortSpec portSpec = new DockerPortSpec("8080", "80");
        //when
        String exposedPort = portSpec.exposedPort();
        //then
        assertEquals("80", exposedPort);
    }
}
package info.scce.gantry.impl;

import info.scce.gantry.*;
import info.scce.gantry.fake.*;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Collections;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DockerProjectTest {
    @Test
    void hostDirectory() {
        //given
        Project project = new DockerProject(new File("/tmp"), "test");
        //then
        assertEquals(
                "/tmp/hostPath:containerPath",
                //when
                project
                        .hostDirectory("hostPath", "containerPath")
                        .serialized()
        );
    }

    @Test
    void providedVolume() {
        //given
        Project project = new DockerProject(new File("/tmp"), "test");
        ProvidedVolume providedVolume = project.providedVolume("volume", "containerPath");
        //then
        assertEquals(
                "test_volume",
                //when
                providedVolume.name()
        );
        assertEquals(
                "containerPath",
                //when
                providedVolume.containerPath()
        );
    }

    @Test
    void containerName() {
        //given
        Project project = new DockerProject(new File("/tmp"), "test");
        //then
        assertEquals(
                "test_name",
                //when
                project
                        .containerName("name")
                        .name()
        );
    }

    @Test
    void pulledImage() {
        //given
        Project project = new DockerProject(new File("/tmp"), "test");
        Image image = project.pulledImage("name");
        //then
        assertEquals(
                "name",
                //when
                image
                        .imageName()
                        .name()
        );
    }

    @Test
    void pristineEnvironment() {
        //given
        Project project = new DockerProject(new File("/tmp"), "test");
        Container container = new FakeContainer(FakeContainerStatus.simple(), new FakeContainerName("test_name"));
        //then
        assertEquals(
                container,
                //when
                project
                        .environment(container)
                        .container("name")
        );
    }

    @Test
    void entrypoint() {
        //given
        Project project = new DockerProject(new File("/tmp"), "test");
        //then
        assertEquals(
                "postgres",
                //when
                project
                        .entrypoint("postgres")
                        .stream()
                        .map(Entrypoint::serialized)
                        .collect(Collectors.joining())
        );
    }

    @Test
    void commands() {
        //given
        Project project = new DockerProject(new File("/tmp"), "test");
        //then
        assertEquals(
                "-c foo=bar",
                //when
                project
                        .command("-c foo=bar")
                        .stream()
                        .map(Command::serialized)
                        .collect(Collectors.joining())
        );
    }

    @Test
    void portSpec() {
        //given
        Project project = new DockerProject(new File("/tmp"), "test");
        //then
        assertEquals(
                "host:container",
                //when
                project
                        .portSpec("host", "container")
                        .serialized()
        );
    }

    @Test
    void environmentVariable() {
        //given
        Project project = new DockerProject(new File("/tmp"), "test");
        //then
        assertEquals(
                "KEY=VALUE",
                //when
                project
                        .environmentVariable("KEY", "VALUE")
                        .serialized()
        );
    }

    @Test
    void link() {
        //given
        Project project = new DockerProject(new File("/tmp"), "test");
        //when
        Link link = project.link("link");
        //then
        assertEquals("test_link", link.name());
        assertEquals("link", link.alias());
    }

    @Test
    void builtImage() {
        //given
        Project project = new DockerProject(new File("/tmp"), "test");
        //then
        assertEquals(
                "test_image",
                //when
                project
                        .builtImage("image")
                        .imageName()
                        .name()
        );
    }

    @Test
    void builtImageSubfolder() {
        //given
        Project project = new DockerProject(new File("/tmp"), "test");
        //then
        assertEquals(
                "test_image",
                //when
                project
                        .builtImageSubfolder("image")
                        .imageName()
                        .name()
        );
    }

    @Test
    void containerConfig() {
        //given
        Project project = new DockerProject(new File("/tmp"), "test");
        //when
        ContainerConfig containerConfig = project.containerConfig(
                Collections.singletonList(FakeEntrypoint.simple()),
                Collections.singletonList(FakeCommand.simple()),
                Collections.singletonList(FakeEnvironmentVariable.simple()),
                Collections.singletonList(FakeLink.simple()),
                Collections.singletonList(FakePortSpec.simple()),
                Collections.singletonList(FakeHostDirectory.simple()),
                Collections.singletonList(FakeProvidedVolume.simple())
        );
        //then
        assertEquals(1, containerConfig.cmds().size());
        assertEquals(1, containerConfig.env().size());
        assertEquals(1, containerConfig.links().size());
        assertEquals(1, containerConfig.portBindings().size());
        assertEquals(2, containerConfig.binds().size());
    }

    @Test
    void container() {
        //given
        Project project = new DockerProject(new File("/tmp"), "test");
        //when
        Container container = project.container(
                FakeImage.simple(),
                FakeContainerName.simple(),
                FakeContainerConfig.simple()
        );
        //then
        assertEquals("name", container.containerName().name());
    }
}
package info.scce.gantry.impl;

import info.scce.gantry.ProvidedVolume;
import info.scce.gantry.impl.api.fake.FakeApiVolume;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DockerProvidedVolumeTest {
    @Test
    void name() {
        //given
        DockerProvidedVolume dockerProvidedVolume = new DockerProvidedVolume(FakeApiVolume.simple(), "name", "/path");
        //then
        assertEquals(
                "name",
                //when
                dockerProvidedVolume.name()
        );
        assertEquals(
                "/path",
                //when
                dockerProvidedVolume.containerPath()
        );
    }

    @Test
    void remove() throws ProvidedVolume.ProvidedVolumeException {
        //given
        FakeApiVolume volume = FakeApiVolume.simple();
        DockerProvidedVolume dockerProvidedVolume = new DockerProvidedVolume(
                volume,
                "name",
                "/path"
        );
        //when
        dockerProvidedVolume.remove();
        //then
        assertTrue(volume.existsHasBeenCalled());
        assertTrue(volume.removeHasBeenCalled());
    }

    @Test
    void removeAbsentVolume() throws ProvidedVolume.ProvidedVolumeException {
        //given
        FakeApiVolume volume = FakeApiVolume.absent();
        DockerProvidedVolume dockerProvidedVolume = new DockerProvidedVolume(
                volume,
                "name",
                "/path"
        );
        //when
        dockerProvidedVolume.remove();
        //then
        assertTrue(volume.existsHasBeenCalled());
        assertFalse(volume.removeHasBeenCalled());
    }

    @Test
    void removeFailed() {
        //given
        FakeApiVolume volume = FakeApiVolume.fail();
        DockerProvidedVolume dockerProvidedVolume = new DockerProvidedVolume(
                volume,
                "name",
                "/path"
        );
        try {
            //when
            dockerProvidedVolume.remove();
            fail(String.format("Expect %s to be thrown", ProvidedVolume.ProvidedVolumeException.class));
        } catch (ProvidedVolume.ProvidedVolumeException ignored) {
            //then
            assertTrue(volume.existsHasBeenCalled());
            assertTrue(volume.removeHasBeenCalled());
        }
    }
}
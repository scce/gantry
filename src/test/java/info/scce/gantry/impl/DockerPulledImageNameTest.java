package info.scce.gantry.impl;

import info.scce.gantry.ImageName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DockerPulledImageNameTest {
    @Test
    void name() {
        //given
        ImageName imageName = new DockerPulledImageName("nginx:latest");
        //then
        assertEquals(
                "nginx:latest",
                //when
                imageName.name()
        );
    }
}
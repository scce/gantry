package info.scce.gantry.impl;

import info.scce.gantry.Image;
import info.scce.gantry.fake.FakeImageName;
import info.scce.gantry.impl.api.fake.FakeApiPullImage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

class DockerPulledImageTest {
    @Test
    void prepare() throws Image.ImageException {
        //given
        FakeApiPullImage fakeApiPullImage = FakeApiPullImage.simple();
        Image image = new DockerPulledImage(fakeApiPullImage, FakeImageName.simple());
        //when
        image.prepare();
        //then
        assertTrue(fakeApiPullImage.pullHasBeenCalled());
    }

    @Test
    void prepareFailed() {
        //given
        FakeApiPullImage fakeApiPullImage = FakeApiPullImage.fail();
        Image image = new DockerPulledImage(fakeApiPullImage, FakeImageName.simple());
        try {
            //when
            image.prepare();
            fail(String.format("Expect %s to be thrown", Image.ImageException.class));
        } catch (Image.ImageException ignored) {
            //then
            assertTrue(fakeApiPullImage.pullHasBeenCalled());
        }
    }

    @Test
    void remove() throws Image.ImageException {
        //given
        FakeApiPullImage fakeApiPullImage = FakeApiPullImage.simple();
        Image image = new DockerPulledImage(fakeApiPullImage, FakeImageName.simple());
        //when
        image.remove();
        //then
        assertTrue(fakeApiPullImage.removeHasBeenCalled());
    }

    @Test
    void removeFailed() {
        //given
        FakeApiPullImage fakeApiPullImage = FakeApiPullImage.fail();
        Image image = new DockerPulledImage(fakeApiPullImage, FakeImageName.simple());
        try {
            //when
            image.remove();
            fail(String.format("Expect %s to be thrown", Image.ImageException.class));
        } catch (Image.ImageException ignored) {
            //then
            assertTrue(fakeApiPullImage.removeHasBeenCalled());
        }
    }
}
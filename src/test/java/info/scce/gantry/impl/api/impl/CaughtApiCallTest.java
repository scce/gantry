package info.scce.gantry.impl.api.impl;

import com.github.dockerjava.api.exception.DockerException;
import info.scce.gantry.impl.api.ApiException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CaughtApiCallTest {
    @Test
    void call() throws ApiException {
        //given
        CaughtApiCall<Boolean> caughtApiCall = CaughtApiCall.of(() -> true);
        //when
        assertTrue(
                //then
                caughtApiCall.call()
        );
    }

    @Test
    void callCaught() {
        //given
        CaughtApiCall<Boolean> caughtApiCall = CaughtApiCall.of(() -> {
            throw new DockerException("Test", 500);
        });
        //when
        try {
            caughtApiCall.call();
            fail(String.format("Expect %s to be thrown", ApiException.class));
        } catch (ApiException e) {
            //then
            assertEquals("The API call failed unexpectedly", e.getMessage());
            assertEquals("Status 500: Test", e.getCause().getMessage());
        }
    }
}
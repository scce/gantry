package info.scce.gantry.impl.api.impl;

import com.github.dockerjava.api.command.CreateContainerCmd;
import com.github.dockerjava.api.model.*;
import info.scce.gantry.fake.FakeContainerConfig;
import info.scce.gantry.fake.FakeContainerName;
import info.scce.gantry.fake.FakeImage;
import info.scce.gantry.impl.DefaultDockerClientFactory;
import info.scce.gantry.impl.DockerClientFactory;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CreateContainerCmdBuilderTest {

    @Test
    void builtSimpleContainerConfig() {
        //given
        DockerClientFactory factory = new DefaultDockerClientFactory();
        CreateContainerCmdBuilder builder = CreateContainerCmdBuilder.of(
                factory.build(),
                FakeImage.simple(),
                FakeContainerName.simple(),
                FakeContainerConfig.simple()
        );
        //when
        try (CreateContainerCmd cmd = builder.built()) {
            //then
            assertEquals("name", cmd.getName());
            assertEquals("simple", cmd.getImage());
            assertNull(cmd.getEnv());
            assertNull(cmd.getCmd());
            assertNull(cmd.getEntrypoint());
            assertNotNull(cmd.getExposedPorts());
            assertEquals(0, cmd.getExposedPorts().length);
            assertNotNull(cmd.getHostConfig());
            assertNotNull(cmd.getHostConfig().getBinds());
            assertEquals(0, cmd.getHostConfig().getBinds().length);
            assertNotNull(cmd.getHostConfig().getLinks());
            assertEquals(0, cmd.getHostConfig().getLinks().length);
            assertNull(cmd.getHostConfig().getPortBindings());
        }

    }

    @Test
    void builtComplexContainerConfig() {
        //given
        DockerClientFactory factory = new DefaultDockerClientFactory();
        CreateContainerCmdBuilder builder = CreateContainerCmdBuilder.of(
                factory.build(),
                FakeImage.simple(),
                FakeContainerName.simple(),
                new FakeContainerConfig(
                        Collections.singletonList(
                                "entrypoint"
                        ),
                        Collections.singletonList(
                                "cmd"
                        ),
                        Collections.singletonList(
                                Bind.parse("volumeName:containerPath")
                        ),
                        Collections.singletonList(
                                "serialized"
                        ),
                        Collections.singletonList(
                                PortBinding.parse("8080:80")
                        ),
                        Collections.singletonList(
                                ExposedPort.parse("80")
                        ),
                        Collections.singletonList(
                                Link.parse("name:alias")
                        ),
                        false
                )
        );
        //when
        try (CreateContainerCmd cmd = builder.built()) {
            //then
            assertEquals("name", cmd.getName());
            assertEquals("simple", cmd.getImage());

            assertNotNull(cmd.getEntrypoint());
            assertEquals("entrypoint", cmd.getEntrypoint()[0]);

            assertNotNull(cmd.getCmd());
            assertEquals("cmd", cmd.getCmd()[0]);

            assertNotNull(cmd.getEnv());
            assertEquals("serialized", cmd.getEnv()[0]);

            assertNotNull(cmd.getHostConfig());
            assertNotNull(cmd.getHostConfig().getBinds());
            assertEquals(1, cmd.getHostConfig().getBinds().length);
            assertEquals("volumeName:containerPath:rw", cmd.getHostConfig().getBinds()[0].toString());

            assertNotNull(cmd.getHostConfig().getLinks());
            assertEquals(1, cmd.getHostConfig().getLinks().length);

            assertNotNull(cmd.getExposedPorts());
            assertEquals(1, cmd.getExposedPorts().length);
            assertEquals(80, cmd.getExposedPorts()[0].getPort());

            assertNotNull(cmd.getHostConfig().getPortBindings());
            Map<ExposedPort, Ports.Binding[]> bindings = cmd.getHostConfig().getPortBindings().getBindings();
            assertEquals("8080", bindings.get(ExposedPort.parse("80"))[0].toString());
        }

    }
}